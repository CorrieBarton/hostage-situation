﻿using UnityEngine;


struct NodeStruct
{

    public int distanceToTarget;
    public int distanceToOrigin;
    public Vector2 pos;
    public Vector2 parentPos;

}

