﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateMachine : MonoBehaviour {


    [SerializeField]
    private EnemyStateEnum currentState;

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private GameObject[] players;

    [SerializeField]
    private Vector3 playerLastKnownPos;

    [SerializeField]
    private bool tempBool = false;

    private GameObject ray;


    private bool lookAround = false;
    [SerializeField]
    private float shootTimer;

	// Use this for initialization
	void Start () {

        players = GameObject.FindGameObjectsWithTag("Player");
        ray = transform.FindChild("ray").gameObject;
	}
	
	// Update is called once per frame
	void Update () {

        switch (currentState)
        {
            case EnemyStateEnum.Guard:
                //guard is when an enemy is in a room
                //get random tile in room
                //move to that positon
                //wait so many seconds
                //repeat
                break;
            case EnemyStateEnum.Patrol:
                //patrol is when an enemy is patroling a corridor
                //go from one end to the other
                //will need some path finding when going round corners (or just make them not do that?)
                break;
            case EnemyStateEnum.Chase:
                //print("I'm chasing");

                transform.position = Vector3.MoveTowards(transform.position, (player.transform.position), 0.01f);
                //change to rotate slower
                transform.right = player.transform.position - transform.position;
                break;
            case EnemyStateEnum.Search:
                //search rooms?

                if (lookAround)
                {

                }

                break;
            case EnemyStateEnum.Alert:
                //
                break;
            case EnemyStateEnum.Shoot:
                //Shoot at the player

                if (tempBool)
                {
                    if (shootTimer >= 0.1)
                    {
                        GetComponent<EnemyWeapon>().Shoot();
                        shootTimer = 0;
                    }
                    else
                        shootTimer += Time.deltaTime;
                }
                else
                {
                    currentState = EnemyStateEnum.Chase;
                }

                break;
        }

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            tempBool = true;

            player = other.gameObject;
            RaycastHit2D hit = Physics2D.Raycast(ray.transform.position, player.transform.position - ray.transform.position);
            if (hit.collider != null)
            {
                if (hit.transform == player.transform)
                {
                    if (GetComponent<EnemyPathFinding>().IsSearching())
                    {
                        GetComponent<EnemyPathFinding>().StopSearching();
                    }

                    currentState = EnemyStateEnum.Chase;
                    playerLastKnownPos = player.transform.position;
                    Shoot();
                }
                else
                {
                    if (currentState == EnemyStateEnum.Chase || currentState == EnemyStateEnum.Shoot)
                    {
                        print("set goal: " + playerLastKnownPos);
                        GetComponent<EnemyPathFinding>().SetGoal(playerLastKnownPos);
                        currentState = EnemyStateEnum.Search;
                    }
                }
            }
        }
    }

    //void OnTriggerExit2D(Collider2D other)
    //{
    //    if (other.CompareTag("Player"))
    //    {
    //        tempBool = false;
    //        //player has left cone of vision, do stuff
    //        if (currentState == EnemyStateEnum.Chase)
    //        {
    //            GetComponent<EnemyPathFinding>().SetGoal(playerLastKnownPos);
    //            currentState = EnemyStateEnum.Search;
    //        }
    //    }
    //}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            FacePlayer(other.transform.position);
        }
    }

    public void FacePlayer(Vector3 source)
    {
        transform.right = source - transform.position;
    }

    void Shoot()
    {
        if (shootTimer >= 0.5)
        {
            GetComponent<EnemyWeapon>().Shoot();
            shootTimer = 0;
        }
        else
            shootTimer += Time.deltaTime;
    }
}
