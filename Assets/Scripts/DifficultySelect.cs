﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultySelect : MonoBehaviour {

    [SerializeField]
    private int difficultySelect = 0;
    [SerializeField]
    private bool action = false;

    [SerializeField]
    private GameObject playerSelection;
    [SerializeField]
    private GameObject[] selections;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


        if (Input.GetAxis("Back1") > 0 )
        {
            if (GameObject.Find("GameManager") != null)
                GameObject.Find("GameManager").GetComponent<LoadScene>().LoadNewScene(1);
        }

        if (Input.GetAxisRaw("DiffSelect") > 0)
        {
            if (!action)
            {
                difficultySelect++;
                if (difficultySelect > 2)
                    difficultySelect = 0;

                action = true;
            }
        }
        else if (Input.GetAxisRaw("DiffSelect") < 0)
        {
            if (!action)
            {
                difficultySelect--;
                if (difficultySelect < 0)
                    difficultySelect = 2;

                action = true;
            }
        }
        else
        {
            action = false;
        }


        if (difficultySelect == 0)
        {
            playerSelection.transform.position = selections[0].transform.position;
        }
        else if (difficultySelect == 1)
        {
            playerSelection.transform.position = selections[1].transform.position;
        }
        else
        {
            playerSelection.transform.position = selections[2].transform.position;
        }


        if (Input.GetButtonDown("Action1"))
        {
            GameObject.Find("GameManager").GetComponent<GameSettings>().SetDifficulty(difficultySelect);
            GameObject.Find("GameManager").GetComponent<LoadScene>().LoadNewScene(3);
        }


    }
}
