﻿using UnityEngine;
using System.Collections;

public class CharacterHit : MonoBehaviour
{

    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Start()
    {

        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //when hit by something, call Hit()
    }

    public void Hit(Vector3 source)
    {
        if (gameObject.CompareTag("Enemy"))
        {
            //currently set to 5, will need to pass in the damage taken from bullet script
            GetComponent<EnemyHealth>().TakeDamage(5);

            GetComponent<EnemyStateMachine>().FacePlayer(source);


        }
        else if (gameObject.CompareTag("Hostage"))
        {
            GetComponent<EnemyHealth>().TakeDamage(5);
        }


        StartCoroutine(SpriteChange());
    }

    IEnumerator SpriteChange()
    {
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.2f);
        spriteRenderer.color = Color.white;
    }
}
