﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPoolSystem : MonoBehaviour {

    [SerializeField]
    private GameObject bulletPrefab;

    [SerializeField]
    private GameObject[] bullets;

	// Use this for initialization
	void Start () {

        for (int i = 0; i < bullets.Length; i++)
        {
            GameObject temp = Instantiate(bulletPrefab, transform);

            temp.SetActive(false);

            bullets[i] = temp;
            //temp.transform.parent = transform;
        }

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject GetBullet()
    {
        for (int i = 0; i < bullets.Length; i++)
        {
            if (bullets[i].activeInHierarchy == false)
            {
                return bullets[i];
            }
        }

        return null;
    }
}
