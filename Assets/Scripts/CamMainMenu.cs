﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMainMenu : MonoBehaviour {

    [SerializeField]
    private GenerateBuilding generate;

    [SerializeField]
    private GameObject BuildingGenerator;

	// Use this for initialization
	void Start () {

        gameObject.transform.position = (generate.GetBuildingSize() * 0.63f) / 2;

        gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.Rotate(Vector3.forward / 2);
		
	}
}
