﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    [SerializeField]
    private int health;
    [SerializeField]
    private int maxHealth;

    // Use this for initialization
    void Start () {

        health = maxHealth;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetHealth(int value)
    {
        health = value;
        maxHealth = value;
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health < 0)
        {
            health = 0;

            if (gameObject.CompareTag("Hostage"))
            {
                if (GameObject.Find("GameManager") != null)
                {
                    GameObject.Find("GameManager").GetComponent<LoadScene>().LoadNewSceneFade(1);
                }
            }

            //currently just destory the gameobject, will need to change to disable object from a pool.
            Destroy(gameObject);
        }
    }
}
