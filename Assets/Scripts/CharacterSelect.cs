﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelect : MonoBehaviour {


    [SerializeField]
    private int charactersJoined;

    [SerializeField]
    private int character;

    [Header("Character Stats")]
    [SerializeField]
    private int[] maxHealth;
    [SerializeField]
    private int[] maxStamina;
    [SerializeField]
    private int[] maxAmmo;
    [SerializeField]
    private int[] clipSize;
    [SerializeField]
    private GunTypes[] gun;
    [SerializeField]
    private float[] speed;
    [SerializeField]
    private float[] sprintMultiplier;
    [SerializeField]
    private float[] reloadTimer;

	// Use this for initialization
	void Start () {

        maxHealth = new int[4];
        maxStamina = new int[4];
        maxAmmo = new int[4];
        clipSize = new int[4];
        gun = new GunTypes[4];
        speed = new float[4];
        sprintMultiplier = new float[4];
        reloadTimer = new float[4];

        DontDestroyOnLoad(gameObject);
		
	}

    public void SelectCharacter(int chara, int player)
    {
        character = chara;
        //print("character: " + character);
        //print("player: " + player);

        //pistol
        if (chara == 0)
        {
            maxHealth[player] = 100;
            maxStamina[player] = 100;
            speed[player] = 1.5f;
            sprintMultiplier[player] = 1.5f;
            gun[player] = GunTypes.Pistol;
            maxAmmo[player] = 50;
            clipSize[player] = 10;
            reloadTimer[player] = 1.5f;
        }
        //beefy
        else if (chara == 1)
        {
            maxHealth[player] = 200;
            maxStamina[player] = 0;
            speed[player] = 0.5f;
            sprintMultiplier[player] = 1;
            gun[player] = GunTypes.MiniGun;
            maxAmmo[player] = 150;
            clipSize[player] = 50;
            reloadTimer[player] = 3;
        }
        //speed
        else if (chara == 2)
        {
            maxHealth[player] = 50;
            maxStamina[player] = 200;
            speed[player] = 2;
            sprintMultiplier[player] = 1.5f;
            gun[player] = GunTypes.SMG;
            maxAmmo[player] = 100;
            clipSize[player] = 15;
            reloadTimer[player] = 1.5f;
        }
        //hacker
        else
        {
            maxHealth[player] = 100;
            maxStamina[player] = 100;
            speed[player] = 1.5f;
            sprintMultiplier[player] = 1.5f;
            gun[player] = GunTypes.Shotgun;
            maxAmmo[player] = 50;
            clipSize[player] = 3;
            reloadTimer[player] = 1;
        }
    }

    public void SetPlayerAmount(int amount)
    {
        charactersJoined = amount;
    }

    public int GetPlayerAmount()
    {
        return charactersJoined;
    }

    public int GetCharacter()
    {
        return character;
    }

    public int GetMaxHealth(int player)
    {
        return maxHealth[player];
    }

    public int GetMaxStamina(int player)
    {
        return maxStamina[player];
    }

    public int GetMaxAmmo(int player)
    {
        return maxAmmo[player];
    }

    public int GetClipSize(int player)
    {
        return clipSize[player];
    }

    public GunTypes GetGun(int player)
    {
        return gun[player];
    }

    public float GetSpeed(int player)
    {
        return speed[player];
    }
    
    public float GetSprintMultiplier(int player)
    {
        return sprintMultiplier[player];
    }

    public float GetReloadTimer(int player)
    {
        return reloadTimer[player];
    }
}
