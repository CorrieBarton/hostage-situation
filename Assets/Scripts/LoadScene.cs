﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour {

    [SerializeField]
    private bool fadeBool = false;
    [SerializeField]
    private Image blackSprite;

    private Color tempColour;

    private int sceneToLoad;

	// Use this for initialization
	void Start () {

        StopCoroutine(Fade());
		
	}
	
	// Update is called once per frame
	void Update () {

        if (blackSprite == null)
        {
            try
            {
                blackSprite = GameObject.Find("BlackSprite").GetComponent<Image>();
            }
            catch { }
        }

        if (fadeBool)
        {
            tempColour = blackSprite.color;
            tempColour.a += 0.0075f;
            blackSprite.color = tempColour;
        }
		
	}

    public void LoadNewScene(int scene)
    {
        if (GameObject.Find("CharacterSelect") != null && scene == 1)
        {
            Destroy(GameObject.Find("CharacterSelect"));
        }

        SceneManager.LoadScene(scene);

        if (scene == 1)
        {
            Destroy(gameObject);
        }

    }


    public void LoadNewSceneFade(int scene)
    {
        sceneToLoad = scene;
        if (!fadeBool)
            StartCoroutine(Fade());
    }

    IEnumerator Fade()
    {
        fadeBool = true;
        yield return new WaitForSeconds(5);
        fadeBool = false;

        if (GameObject.Find("CharacterSelect") != null && sceneToLoad == 1)
        {
            Destroy(GameObject.Find("CharacterSelect"));
        }

        SceneManager.LoadScene(sceneToLoad);

        if (sceneToLoad == 1)
        {
            Destroy(gameObject);
        }
    }
}
