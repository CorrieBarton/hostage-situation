﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
    private Transform target;
    [SerializeField]
    private float smoothing = 5.0f;


    [SerializeField]
    private Transform[] targets;

    [SerializeField]
    private float tempDist;

    private CharacterSelect characters;

    private GameObject[] players;
    private GameObject[] playersUI;


    void Start()
    {
        if (GameObject.Find("CharacterSelect") != null)
        {
            characters = GameObject.Find("CharacterSelect").GetComponent<CharacterSelect>();
        }

        players = new GameObject[4];
        playersUI = new GameObject[4];

        for (int i = 0; i < players.Length; i++)
        {
            players[i] = GameObject.Find("Character" + (i + 1).ToString());
            players[i].SetActive(false);

            playersUI[i] = GameObject.Find("Player" + (i + 1).ToString());
            playersUI[i].SetActive(false);
        }

        targets = new Transform[characters.GetPlayerAmount()];

        //print(targets.Length);
        //print(GameObject.Find("Character2"));

        for (int i = 0; i < targets.Length; i++)
        {
            targets[i] = players[i].transform;
            players[i].SetActive(true);
            playersUI[i].SetActive(true);
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 tempPos = targets[0].position;

        if (targets.Length > 1)
        {
            tempDist = Vector3.Distance(targets[0].position, targets[1].position);

            for (int i = 1; i < targets.Length; i++)
            {
                tempPos += targets[i].position;

                for (int j = 0; j < targets.Length; j++)
                {
                    if (Vector3.Distance(targets[j].position, targets[i].position) > tempDist)
                    {
                        tempDist = Vector3.Distance(targets[j].position, targets[i].position);
                    }
                }
            }

            tempPos /= targets.Length;

            if (tempDist > 4 && tempDist < 10)
            {
                    GetComponent<Camera>().orthographicSize = 2.5f + ((tempDist - 4) / 2.5f);
            }
        }
        else
        {
            if (GetComponent<Camera>().orthographicSize != 2.5f)
            {
                GetComponent<Camera>().orthographicSize = 2.5f;
            }
        }

        tempPos = new Vector3(tempPos.x, tempPos.y, transform.position.z);

        transform.position = Vector3.Lerp(transform.position, tempPos, (smoothing * 0.001f));


        //Vector3 newPos = new Vector3(target.position.x, target.position.y, transform.position.z);
        //transform.position = Vector3.Lerp(transform.position, newPos, (smoothing * 0.001f));
    }

    public void RemovePlayer(GameObject player)
    {
        if (targets.Length > 1)
        {
            Transform[] tempTargets = new Transform[targets.Length - 1];

            int tempi = 0;

            for (int i = 0; i < targets.Length; i++)
            {
                if (targets[i] != player.transform)
                {
                    tempTargets[tempi] = targets[i];
                    tempi++;
                }
            }


            targets = tempTargets;
        }
        else
        {
            if (GameObject.Find("GameManager") != null)
                GameObject.Find("GameManager").GetComponent<LoadScene>().LoadNewSceneFade(1);
        }

    }
}
