﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectedPlayers : MonoBehaviour {

    [SerializeField]
    private bool[] playerConnected;

    [SerializeField]
    private int[] playerSelected;

    [SerializeField]
    private GameObject[] playerOutline;

    [SerializeField]
    private GameObject[] selection;

    [SerializeField]
    private bool[] playerAction;

    [SerializeField]
    private bool[] playerChosen;

    [SerializeField]
    private LoadScene loadScene;
    [SerializeField]
    private CharacterSelect charSelect;

    // Use this for initialization
    void Start () {

        playerConnected = new bool[4];
        playerSelected = new int[4];
        playerAction = new bool[4];
        playerChosen = new bool[4];
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButtonDown("Action" + 1))
        {
            if (!playerConnected[0])
            {
                playerConnected[0] = true;
                playerOutline[0].SetActive(true);
            }
            else
            {
                print("player 1 selected: " + playerSelected[0]);

                playerChosen[0] = true;
                playerOutline[0].GetComponent<Image>().color = Color.gray;
            }
        }

        if (Input.GetButtonDown("Action" + 2))
        {
            if (!playerConnected[1])
            {
                playerConnected[1] = true;
                playerOutline[1].SetActive(true);
            }
            else
            {
                print("player 2 selected: " + playerSelected[1]);

                playerChosen[1] = true;
                playerOutline[1].GetComponent<Image>().color = Color.gray;
            }
        }

        if (Input.GetButtonDown("Action" + 3))
        {
            if (!playerConnected[2])
            {
                playerConnected[2] = true;
                playerOutline[2].SetActive(true);
            }
            else
            {
                print("player 3 selected: " + playerSelected[2]);

                playerChosen[2] = true;
                playerOutline[2].GetComponent<Image>().color = Color.gray;
            }
        }

        if (Input.GetButtonDown("Action" + 4))
        {
            if (!playerConnected[3])
            {
                playerConnected[3] = true;
                playerOutline[3].SetActive(true);
            }
            else
            {
                print("player 4 selected: " + playerSelected[3]);

                playerChosen[3] = true;
                playerOutline[3].GetComponent<Image>().color = Color.gray;
            }
        }

        if (Input.GetAxisRaw("Select" + 1) > 0)
        {
            if (!playerAction[0] && !playerChosen[0])
            {
                playerSelected[0]++;
                if (playerSelected[0] > 3)
                    playerSelected[0] = 0;

                playerAction[0] = true;
            }
        }
        else if (Input.GetAxisRaw("Select" + 1) < 0)
        {
            if (!playerAction[0] && !playerChosen[0])
            {
                playerSelected[0]--;
                if (playerSelected[0] < 0)
                    playerSelected[0] = 3;

                playerAction[0] = true;
            }
        }
        else
        {
            playerAction[0] = false;
        }

        if (Input.GetAxisRaw("Select" + 2) > 0)
        {
            if (!playerAction[1] && !playerChosen[1])
            {
                playerSelected[1]++;
                if (playerSelected[1] > 3)
                    playerSelected[1] = 0;

                playerAction[1] = true;
            }
        }
        else if (Input.GetAxisRaw("Select" + 2) < 0)
        {
            if (!playerAction[1] && !playerChosen[1])
            {
                playerSelected[1]--;
                if (playerSelected[1] < 0)
                    playerSelected[1] = 3;

                playerAction[1] = true;
            }
        }
        else
        {
            playerAction[1] = false;
        }

        if (Input.GetAxisRaw("Select" + 3) > 0)
        {
            if (!playerAction[2] && !playerChosen[2])
            {
                playerSelected[2]++;
                if (playerSelected[2] > 3)
                    playerSelected[2] = 0;

                playerAction[2] = true;
            }
        }
        else if (Input.GetAxisRaw("Select" + 3) < 0)
        {
            if (!playerAction[2] && !playerChosen[2])
            {
                playerSelected[2]--;
                if (playerSelected[2] < 0)
                    playerSelected[2] = 3;

                playerAction[2] = true;
            }
        }
        else
        {
            playerAction[2] = false;
        }

        if (Input.GetAxisRaw("Select" + 4) > 0)
        {
            if (!playerAction[3] && !playerChosen[3])
            {
                playerSelected[3]++;
                if (playerSelected[3] > 3)
                    playerSelected[3] = 0;

                playerAction[3] = true;
            }
        }
        else if (Input.GetAxisRaw("Select" + 4) < 0)
        {
            if (!playerAction[3] && !playerChosen[3])
            {
                playerSelected[3]--;
                if (playerSelected[3] < 0)
                    playerSelected[3] = 3;

                playerAction[3] = true;
            }
        }
        else
        {
            playerAction[3] = false;
        }

        if (Input.GetButtonDown("Start"))
        {
            int amount = 0;

            for (int i = 0; i < playerConnected.Length; i++)
            {
                if (playerConnected[i])
                {
                    amount++;
                }
            }

            if (amount > 0)
            {
                charSelect.SetPlayerAmount(amount);

                for (int i = 0; i < amount; i++)
                {
                    charSelect.SelectCharacter(playerSelected[i], i);
                }

                loadScene.LoadNewScene(2);
            }
        }



        //Outline positioning
        if (playerSelected[0] == 0)
        {
            playerOutline[0].transform.position = selection[0].transform.position;
        }
        else if (playerSelected[0] == 1)
        {
            playerOutline[0].transform.position = selection[1].transform.position;
        }
        else if (playerSelected[0] == 2)
        {
            playerOutline[0].transform.position = selection[2].transform.position;
        }
        else
        {
            playerOutline[0].transform.position = selection[3].transform.position;
        }

        if (playerSelected[1] == 0)
        {
            playerOutline[1].transform.position = selection[0].transform.position;
        }
        else if (playerSelected[1] == 1)
        {
            playerOutline[1].transform.position = selection[1].transform.position;
        }
        else if (playerSelected[1] == 2)
        {
            playerOutline[1].transform.position = selection[2].transform.position;
        }
        else
        {
            playerOutline[1].transform.position = selection[3].transform.position;
        }

        if (playerSelected[2] == 0)
        {
            playerOutline[2].transform.position = selection[0].transform.position;
        }
        else if (playerSelected[2] == 1)
        {
            playerOutline[2].transform.position = selection[1].transform.position;
        }
        else if (playerSelected[2] == 2)
        {
            playerOutline[2].transform.position = selection[2].transform.position;
        }
        else
        {
            playerOutline[2].transform.position = selection[3].transform.position;
        }

        if (playerSelected[3] == 0)
        {
            playerOutline[3].transform.position = selection[0].transform.position;
        }
        else if (playerSelected[3] == 1)
        {
            playerOutline[3].transform.position = selection[1].transform.position;
        }
        else if (playerSelected[3] == 2)
        {
            playerOutline[3].transform.position = selection[2].transform.position;
        }
        else
        {
            playerOutline[3].transform.position = selection[3].transform.position;
        }

    }
}
