﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {

    [SerializeField]
    private Vector2 tilePos;

    [SerializeField]
    private bool isTaken = true;

    [SerializeField]
    private int weighting;
    [SerializeField]
    private bool hasChecked = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetIsTaken(bool flag)
    {
        isTaken = flag;
    }

    public void SetPos(Vector2 p)
    {
        tilePos = p;
    }

    public void setWeighting(int w)
    {
        weighting = w;
    }

    public void SetHasChecked(bool flag)
    {
        hasChecked = flag;
    }

    public bool GetIsTaken()
    {
        return isTaken;
    }

    public Vector2 GetPos()
    {
        return tilePos;
    }

    public int GetWeighting()
    {
        return weighting;
    }

    public bool GetChecked()
    {
        return hasChecked;
    }
    
}
