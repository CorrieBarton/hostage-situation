﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathFinding : MonoBehaviour {


    [SerializeField]
    private Vector2 currentPos;

    [SerializeField]
    private GameObject[,] tiles;

    [SerializeField]
    private GameObject goal;
    [SerializeField]
    private Vector2 buildingMax;


    [SerializeField]
    private List<NodeStruct> checkedList = new List<NodeStruct>();
    [SerializeField]
    private List<NodeStruct> uncheckedList = new List<NodeStruct>();

    [SerializeField]
    private bool searching = false;

    [SerializeField]
    private bool found = false;
    private NodeStruct current;


    [SerializeField]
    private Vector2 ActualGoal;

    private int breakout;

    // Use this for initialization
    void Start () {

        breakout = 0;
	}
	
	// Update is called once per frame
	void Update () {

        //currentPos.x = (float)System.Math.Round(transform.position.x / 0.63f);
        //currentPos.y = (float)System.Math.Round(transform.position.y / 0.63f);


        //if (uncheckedList.Count == 1)
        //    Check4ClosesTiles(uncheckedList[0]);

        if (Input.GetKeyDown(KeyCode.G))
        {
            //print(uncheckedList[0].pos);
            Check4ClosesTiles(uncheckedList[0]);
        }

        while (searching && !found)
        {
            //for (int i = 0; i < uncheckedList.Count; i++)
            //{
            //    Check4ClosesTiles(uncheckedList[i]);
            //}
            try
            {
                Check4ClosesTiles(uncheckedList[0]);
            }
            catch
            {
                print("caught issue with Check4CloseTiles");
            }

            breakout++;

            if (breakout >= 100)
            {
                print("broke out");
                breakout = 0;
                searching = false;
                break;
            }

        }

        if (found)
        {
            MoveToGoal();
        }


    }

    public void SetPosition(Vector2 pos)
    {
        currentPos = pos;
    }

    public void SetTileArray(GameObject[,] t)
    {
        tiles = t;
    }

    public void SetGoal(GameObject g, Vector2 max)
    {
        currentPos.x = (float)System.Math.Round(transform.position.x / 0.63f);
        currentPos.y = (float)System.Math.Round(transform.position.y / 0.63f);

        uncheckedList.Clear();
        checkedList.Clear();
        found = false;

        goal = g;

        NodeStruct temp = new NodeStruct();
        temp.pos = g.GetComponent<Node>().GetPos();
        temp.distanceToTarget = 0;
        temp.distanceToOrigin = (int)Vector2.Distance(temp.pos, currentPos);
        temp.parentPos = temp.pos;

        buildingMax = max;

        uncheckedList.Add(temp);
        //searching = true;
    }

    public void SetGoal(Vector3 g)
    {
        currentPos.x = (float)System.Math.Round(transform.position.x / 0.63f);
        currentPos.y = (float)System.Math.Round(transform.position.y / 0.63f);


        ActualGoal.x = g.x / 0.63f;
        ActualGoal.y = g.y / 0.63f;

        //print(g.x / 0.63f);
        //print(System.Math.Round(g.x / 0.63f));
        //print(Mathf.FloorToInt(g.x));
        //print("--------------------------");
        //print(g.y / 0.63f);
        //print(System.Math.Round(g.y / 0.63f));
        //print(Mathf.FloorToInt(g.y));

        uncheckedList.Clear();
        checkedList.Clear();
        found = false;

        NodeStruct temp = new NodeStruct();
        temp.pos = new Vector2((float)System.Math.Round(g.x / 0.63f), (float)System.Math.Round(g.y / 0.63f));
        temp.distanceToTarget = 0;
        temp.distanceToOrigin = (int)Vector2.Distance(temp.pos, currentPos);
        temp.parentPos = temp.pos;

        uncheckedList.Add(temp);
        searching = true;
    }

    public void StopSearching()
    {
        searching = false;
        found = false;
    }

    public bool IsSearching()
    {
        return searching;
    }

    void Check4ClosesTiles(NodeStruct node)
    {
        //very messy, code cleanup at some point
        GameObject tempNode;
        NodeStruct tempStruct = new NodeStruct();

        //actually check if the temp node is in the checked list, if it is, dont touch it

        if ((node.pos.x - 1) >= 0 )
        {
            tempNode = tiles[(int)node.pos.x - 1, (int)node.pos.y];
            //Debug.DrawRay(tempNode.transform.position, Vector3.right * 0.63f, Color.red, 5.0f);

            tempStruct.pos = tempNode.GetComponent<Node>().GetPos();

            if (!IsInCheckedList(tempStruct))
            {
                tempStruct.distanceToTarget = node.distanceToTarget + tempNode.GetComponent<Node>().GetWeighting();
                tempStruct.distanceToOrigin = (int)Vector2.Distance(tempStruct.pos, currentPos);
                tempStruct.parentPos = node.pos;

                if (tempNode.CompareTag("Wall"))
                {
                    checkedList.Add(tempStruct);
                }
                else
                {
                    if (tempStruct.distanceToOrigin == 0)
                    {
                        //print("I'm at the enemy");
                        current = tempStruct;
                        found = true;
                        //return;
                    }
                    uncheckedList.Add(tempStruct);
                    Debug.DrawRay(tempNode.transform.position, Vector3.right * 0.63f, Color.red, 5.0f);
                }
            }

        }

        if (node.pos.x + 1 < buildingMax.x)
        {
            tempNode = tiles[(int)node.pos.x + 1, (int)node.pos.y];
            //Debug.DrawRay(tempNode.transform.position, Vector3.left * 0.63f, Color.red, 5.0f);

            tempStruct.pos = tempNode.GetComponent<Node>().GetPos();

            if (!IsInCheckedList(tempStruct))
            {
                tempStruct.distanceToTarget = node.distanceToTarget + tempNode.GetComponent<Node>().GetWeighting();
                tempStruct.distanceToOrigin = (int)Vector2.Distance(tempStruct.pos, currentPos);
                tempStruct.parentPos = node.pos;

                if (tempNode.CompareTag("Wall"))
                {
                    checkedList.Add(tempStruct);
                }
                else
                {
                    if (tempStruct.distanceToOrigin == 0)
                    {
                        //print("I'm at the enemy");
                        current = tempStruct;
                        found = true;
                        //return;
                    }
                    uncheckedList.Add(tempStruct);
                    Debug.DrawRay(tempNode.transform.position, Vector3.left * 0.63f, Color.red, 5.0f);
                }
            }

        }

        if (node.pos.y - 1 >= 0)
        {

            tempNode = tiles[(int)node.pos.x, (int)node.pos.y - 1];
            //Debug.DrawRay(tempNode.transform.position, Vector3.up * 0.63f, Color.red, 5.0f);

            tempStruct.pos = tempNode.GetComponent<Node>().GetPos();

            if (!IsInCheckedList(tempStruct))
            {
                tempStruct.distanceToTarget = node.distanceToTarget + tempNode.GetComponent<Node>().GetWeighting();
                tempStruct.distanceToOrigin = (int)Vector2.Distance(tempStruct.pos, currentPos);
                tempStruct.parentPos = node.pos;

                if (tempNode.CompareTag("Wall"))
                {
                    checkedList.Add(tempStruct);
                }
                else
                {
                    if (tempStruct.distanceToOrigin == 0)
                    {
                        //print("I'm at the enemy");
                        current = tempStruct;
                        found = true;
                        //return;
                    }
                    uncheckedList.Add(tempStruct);
                    Debug.DrawRay(tempNode.transform.position, Vector3.up * 0.63f, Color.red, 5.0f);
                }
            }

        }

        if (node.pos.y + 1 < buildingMax.y)
        {
            tempNode = tiles[(int)node.pos.x, (int)node.pos.y + 1];
            //Debug.DrawRay(tempNode.transform.position, Vector3.down * 0.63f, Color.red, 5.0f);

            tempStruct.pos = tempNode.GetComponent<Node>().GetPos();

            if (!IsInCheckedList(tempStruct))
            {
                tempStruct.distanceToTarget = node.distanceToTarget + tempNode.GetComponent<Node>().GetWeighting();
                tempStruct.distanceToOrigin = (int)Vector2.Distance(tempStruct.pos, currentPos);
                tempStruct.parentPos = node.pos;

                if (tempNode.CompareTag("Wall"))
                {
                    checkedList.Add(tempStruct);
                }
                else
                {
                    if (tempStruct.distanceToOrigin == 0)
                    {
                        //print("I'm at the enemy");
                        current = tempStruct;
                        found = true;
                        //return;
                    }
                    uncheckedList.Add(tempStruct);
                    Debug.DrawRay(tempNode.transform.position, Vector3.down * 0.63f, Color.red, 5.0f);
                }
            }
        }


        uncheckedList.Remove(node);
        checkedList.Add(node);

    }

    bool IsInCheckedList(NodeStruct node)
    {
        bool containsNode = false;
        //print("---------------------------");
        //print("temp struct pos: " + node.pos);

        //tends to slow down as list size increases
        for (int i = 0; i < checkedList.Count; i++)
        {
            //print("checked list i pos: " + checkedList[i].pos);
            //print(checkedList[i].pos);
            if (checkedList[i].pos == node.pos)
            {
                containsNode = true;
            }
        }
        //print("---------------------------");
        return containsNode;
    }

    void MoveToGoal()
    {
        if (currentPos == current.pos && current.pos != current.parentPos)
        {
            //transform.position = current.parentPos * 0.63f;

            Vector3 temp = new Vector3(current.parentPos.x, current.parentPos.y, 0);

            transform.right = temp * 0.63f - transform.position;

            if (transform.position != temp * 0.63f)
            {
                transform.position = Vector3.MoveTowards(transform.position, (temp * 0.63f), 0.01f);
            }
            else
            {
                currentPos = current.parentPos;

                for (int i = 0; i < checkedList.Count; i++)
                {
                    //print("searching for the parent: " + checkedList[i].pos);
                    if (checkedList[i].pos == current.parentPos)
                    {
                        //print("found the parent");
                        current = checkedList[i];
                        break;
                    }
                }

            }

            //change to rotate slower
            //transform.right = (current.parentPos * 0.63f) - transform.position;
            //print(current.parentPos);
            //currentPos = current.parentPos;


        }
        else
        {
            //do looking around
            transform.Rotate(Vector3.forward * 50 * Time.deltaTime);
        }
    }
}
