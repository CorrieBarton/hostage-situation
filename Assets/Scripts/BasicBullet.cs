﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBullet : MonoBehaviour {


    private float aliveTime;
    [SerializeField]
    private float timeLimit;

    [SerializeField]
    private float speed;

    [SerializeField]
    private AudioClip shoot;

    private bool firstLoad = true;

	// Use this for initialization
	void Start () {
		
	}

    void OnEnable()
    {
        aliveTime = 0;

    }
	
	// Update is called once per frame
	void Update () {

        transform.position += transform.right * Time.deltaTime * speed;

        aliveTime += Time.deltaTime;

        if (aliveTime >= timeLimit)
            gameObject.SetActive(false);

	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy") || other.CompareTag("Hostage"))
        {
            other.GetComponent<CharacterHit>().Hit(transform.position);
        }
        else if (other.CompareTag("Player") && !transform.CompareTag("Player"))
        {
            other.GetComponent<Health>().TakeDamage(5);
            gameObject.SetActive(false);
        }


        if (!other.CompareTag("Cone") && !other.CompareTag("Bullet") && !other.CompareTag("Player"))
        {
            //print(other.name);
            gameObject.SetActive(false);
        }

    }

    public void PlayBulletSound()
    {
        AudioSource.PlayClipAtPoint(shoot, transform.position);
    }

    public void Activate(string tag)
    {
        transform.tag = tag;
        gameObject.SetActive(true);
    }
}
