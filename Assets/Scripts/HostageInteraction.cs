﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostageInteraction : MonoBehaviour {


    [SerializeField]
    private bool isHeld = false;

    private PlayerController player;

    [SerializeField]
    private GameObject xToInteract;

    private float waitTimer = 0;

    [SerializeField]
    private List<GameObject> enemies = new List<GameObject>();

	// Use this for initialization
	void Start () {

        xToInteract = GameObject.Find("XToInteract");
        xToInteract.SetActive(false);
		
	}
	
	// Update is called once per frame
	void Update () {

        if (player != null)
        {
            if (Input.GetAxis("Interact" + player.GetPlayerNum().ToString()) > 0)
            {
                if (isHeld)
                {
                    transform.parent = null;
                    player.SetHasHostage(false);
                    isHeld = false;

                    waitTimer = 0.0f;
                }
            }
        }

        if (!isHeld)
        {
            waitTimer += Time.deltaTime;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            xToInteract.SetActive(true);

            if (Input.GetAxis("Interact" + other.gameObject.GetComponent<PlayerController>().GetPlayerNum().ToString()) > 0 && waitTimer >= 1)
            {
                print("follow player");

                transform.parent = other.transform;

                player = other.GetComponent<PlayerController>();

                player.SetHasHostage(true);

                for (int i = 0; i < enemies.Count; i++)
                {
                    enemies[i].GetComponent<EnemyPathFinding>().SetGoal(transform.position);
                }

                StartCoroutine(Wait());
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            xToInteract.SetActive(false);
        }
    }

    public void AddEnemy(GameObject enemy)
    {
        print("yeeeee");
        enemies.Add(enemy);
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);

        isHeld = true;
    }

}
