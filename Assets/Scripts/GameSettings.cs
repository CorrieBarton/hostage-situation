﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour {

    [Range(0,2)]
    [SerializeField]
    private int difficulty;

	// Use this for initialization
	void Start () {

        DontDestroyOnLoad(transform.gameObject);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetDifficulty(int diff)
    {
        difficulty = diff;
    }

    public int GetDifficulty()
    {
        return difficulty;
    }
}
