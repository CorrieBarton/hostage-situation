﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour {


    [SerializeField]
    private BulletPoolSystem bulletPool;
    [SerializeField]
    private CharacterStats characterStats;

    //size of each clip
    [SerializeField]
    private int clipSize;
    //amount of ammo in current clip
    [SerializeField]
    private int currentClip;

    [SerializeField]
    private bool isHard;

    [Header("Reloading Variables")]
    [SerializeField]
    private bool isReloading = false;
    [SerializeField]
    private float reloadTimer;
    [SerializeField]
    private float reloadTimerMax;
    [SerializeField]
    private GameObject reloadText;


    //the following variables are for easier modes
    //classic video game reloading system

    [Header("Easy Mode Variables")]
    //max ammo held by player
    [SerializeField]
    private int maxAmmo;
    //current ammo held by player
    [SerializeField]
    private int currentAmmo;

    //following variables are for harder modes
    //used for more realistic reloading system
    [Header("Hard Mode Variables")]
    //max amount of clips can be held
    [SerializeField]
    private int maxClips;
    //current amount of clips held
    [SerializeField]
    private int currentClipAmount;

    [Header("UI Variables")]
    //UI Variables
    [SerializeField]
    private Text currentClipText;

    [Header("Easy UI Variables")]
    [SerializeField]
    private Text currentMaxAmmoText;

    [Header("Hard UI Variables")]
    [SerializeField]
    private Text currentClipAmountText;


    //bullet stuff
    private GameObject tempBullet;

    [Header("Weapon Sounds")]

    private AudioSource audioSource;

    [SerializeField]
    private AudioClip pistolShoot;
    [SerializeField]
    private AudioClip pistolReload;
    [SerializeField]
    private AudioClip pistolNoAmmo;

    [Header("Gun Types")]
    [SerializeField]
    private GunTypes gun;

	// Use this for initialization
	void Start () {

        audioSource = GetComponent<AudioSource>();
        characterStats = GetComponent<CharacterStats>();
        gun = characterStats.GetGun();
        maxAmmo = characterStats.GetMaxAmmo();
        clipSize = characterStats.GetClipSize();
        reloadTimerMax = characterStats.GetReloadTimer();

        currentClip = clipSize;

        GetComponent<PlayerController>().enabled = true;

        if (isHard)
        {
            currentClipAmount = maxClips;

            currentAmmo = -1;
            maxAmmo = -1;

            currentMaxAmmoText.gameObject.SetActive(false);

        }
        else
        {
            currentAmmo = maxAmmo;

            currentClipAmount = -1;
            maxClips = -1;

            currentClipAmountText.gameObject.SetActive(false);
        }
        
        UpdateUI();

    }
	
	// Update is called once per frame
	void Update () {

        if (currentClip == 0 && !reloadText.activeInHierarchy)
        {
            reloadText.SetActive(true);
        }

        if (isReloading)
        {
            if (reloadTimer < reloadTimerMax)
            {
                reloadTimer += Time.deltaTime;
            }
            else
            {
                reloadTimer = 0;
                ReloadAmmo();
            }
        }


		
	}

    public int GetCurrentClipAmount()
    {
        return currentClipAmount;
    }

    public int GetCurrentClip()
    {
        return currentClip;
    }

    public int GetMaxClips()
    {
        return maxClips;
    }

    //harder modes
    public void AddClips(int amount)
    {
        currentClipAmount += amount;
        if (currentClipAmount > maxClips)
            currentClipAmount = maxClips;
    }
    //harder modes
    public void ReloadCurrentClip()
    {

    }
    //harder modes
    public void SetMaxClips()
    {

    }

    //easier modes

    public void StartReloadAmmo()
    {
        if (!isReloading)
        {
            isReloading = true;
            reloadText.GetComponent<Text>().text = "[RELOADING]";
            reloadText.SetActive(true);
        }
    }

    public void ReloadAmmo()
    {

        if (isHard)
        {
            if (currentClipAmount > 0)
            {
                currentClip = clipSize;
                currentClipAmount--;

                audioSource.clip = pistolReload;
                audioSource.Play();
            }

        }
        else
        {
            currentAmmo -= (clipSize - currentClip);
            if (currentAmmo < 0)
            {
                currentClip = (clipSize + currentAmmo);
                currentAmmo = 0;
            }
            else
            {
                currentClip = clipSize;
            }

            audioSource.clip = pistolReload;
            audioSource.Play();

        }

        UpdateUI();

        isReloading = false;
        reloadText.SetActive(false);
        reloadText.GetComponent<Text>().text = "[RELOAD]";
    }

    //easier modes
    public void ShootAmmo(int amount)
    {

        if (currentClip > 0)
        {
            //check gun type

            if (gun == GunTypes.Pistol || gun == GunTypes.MiniGun || gun == GunTypes.SMG)
            {
                tempBullet = bulletPool.GetBullet();
                tempBullet.GetComponent<BasicBullet>().Activate("Player");
                tempBullet.transform.position = transform.GetChild(0).transform.position;
                tempBullet.transform.rotation = transform.GetChild(0).transform.rotation;

                if (gun == GunTypes.MiniGun)
                {
                    float tempAmount = Random.Range(-10,10);
                    tempBullet.transform.Rotate(transform.GetChild(0).transform.rotation.x, transform.GetChild(0).transform.rotation.y, transform.GetChild(0).transform.rotation.z + tempAmount);
                }
                else if (gun == GunTypes.SMG)
                {
                    float tempAmount = Random.Range(-5, 5);
                    tempBullet.transform.Rotate(transform.GetChild(0).transform.rotation.x, transform.GetChild(0).transform.rotation.y, transform.GetChild(0).transform.rotation.z + tempAmount);
                }

                tempBullet.GetComponent<BasicBullet>().PlayBulletSound();
            }
            else if (gun == GunTypes.Shotgun)
            {
                tempBullet = bulletPool.GetBullet();
                tempBullet.GetComponent<BasicBullet>().Activate("Player");
                tempBullet.transform.position = transform.GetChild(0).transform.position;
                tempBullet.transform.rotation = transform.GetChild(0).transform.rotation;

                tempBullet = bulletPool.GetBullet();
                tempBullet.GetComponent<BasicBullet>().Activate("Player");
                tempBullet.transform.position = transform.GetChild(0).transform.position;
                tempBullet.transform.rotation = transform.GetChild(0).transform.rotation;
                tempBullet.transform.Rotate(transform.GetChild(0).transform.rotation.x, transform.GetChild(0).transform.rotation.y, transform.GetChild(0).transform.rotation.z + 25);

                tempBullet = bulletPool.GetBullet();
                tempBullet.GetComponent<BasicBullet>().Activate("Player");
                tempBullet.transform.position = transform.GetChild(0).transform.position;
                tempBullet.transform.rotation = transform.GetChild(0).transform.rotation;
                tempBullet.transform.Rotate(transform.GetChild(0).transform.rotation.x, transform.GetChild(0).transform.rotation.y, transform.GetChild(0).transform.rotation.z - 25);

                tempBullet.GetComponent<BasicBullet>().PlayBulletSound();
            }
        }
        else
        {
            audioSource.clip = pistolNoAmmo;
            audioSource.Play();
        }

        currentClip -= amount;
        if (currentClip < 0)
            currentClip = 0;

        UpdateUI();
    }

    void UpdateUI()
    {
        currentClipText.text = currentClip.ToString();
        currentMaxAmmoText.text = currentAmmo.ToString();
        currentClipAmountText.text = currentClipAmount.ToString();
    }

    public GunTypes GetGun()
    {
        return gun;
    }

    public bool GetReloading()
    {
        return isReloading;
    }
}
