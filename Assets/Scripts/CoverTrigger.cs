﻿using UnityEngine;
using System.Collections;

public class CoverTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //only works once since the objects are set inactive
            //if want to enable again when player leaves room then pls change
            gameObject.SetActive(!gameObject.activeSelf);
        }

    }
}
