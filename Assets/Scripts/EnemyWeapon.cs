﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour {


    [SerializeField]
    private BulletPoolSystem bulletPool;


    private GameObject tempBullet;


    [SerializeField]
    private bool tempShoot = false;

    // Use this for initialization
    void Start () {

        bulletPool = GameObject.Find("BulletPool").GetComponent<BulletPoolSystem>();
		
    }
	
	// Update is called once per frame
	void Update () {

        if (tempShoot)
        {
            Shoot();
            tempShoot = false;
        }
		
	}

    public void Shoot()
    {
        tempBullet = bulletPool.GetBullet();
        tempBullet.GetComponent<BasicBullet>().Activate("Enemy");
        tempBullet.transform.position = transform.GetChild(2).transform.position;
        tempBullet.transform.rotation = transform.GetChild(2).transform.rotation;

        tempBullet.GetComponent<BasicBullet>().PlayBulletSound();
    }
}
