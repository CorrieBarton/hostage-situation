﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
//using UnityEditor;

public class GenerateBuilding : MonoBehaviour
{
    [Header("Walls")]
    [SerializeField]
    private GameObject WallHorizontal;
    [SerializeField]
    private GameObject WallVertical;
    [SerializeField]
    private GameObject WallCornerTopLeft;
    [SerializeField]
    private GameObject WallCornerTopRight;
    [SerializeField]
    private GameObject WallCornerBottomLeft;
    [SerializeField]
    private GameObject WallCornerBottomRight;
    [SerializeField]
    private GameObject WallEdgeLeft;
    [SerializeField]
    private GameObject WallEdgeRight;
    [SerializeField]
    private GameObject WallEdgeTop;
    [SerializeField]
    private GameObject WallEdgeBottom;
    [SerializeField]
    private GameObject WallTriTop;
    [SerializeField]
    private GameObject WallTriBottom;
    [SerializeField]
    private GameObject WallTriLeft;
    [SerializeField]
    private GameObject WallTriRight;

    [Header("Exterior")]
    [SerializeField]
    private GameObject PavementParent;
    [SerializeField]
    private GameObject PavementVertical;
    [SerializeField]
    private GameObject PavementHorizontal;
    [SerializeField]
    private GameObject PavementTopLeft;
    [SerializeField]
    private GameObject PavementTopRight;
    [SerializeField]
    private GameObject PavementBottomLeft;
    [SerializeField]
    private GameObject PavementBottomRight;
    [SerializeField]
    private GameObject PavementOuterCornerBottomLeft;
    [SerializeField]
    private GameObject PavementOuterCornerTopLeft;
    [SerializeField]
    private GameObject PavementOuterCornerTopRight;
    [SerializeField]
    private GameObject PavementOuterCornerBottomRight;

    [SerializeField]
    private GameObject RoadParent;
    [SerializeField]
    private GameObject RoadBlank;
    [SerializeField]
    private GameObject RoadLineHorizontal;
    [SerializeField]
    private GameObject RoadLineVertical;
    [SerializeField]
    private GameObject RoadCornerTopLeft;
    [SerializeField]
    private GameObject RoadCornerTopRight;
    [SerializeField]
    private GameObject RoadCornerBottomLeft;
    [SerializeField]
    private GameObject RoadCornerBottomRight;

    [SerializeField]
    private GameObject WallsParent;
    [SerializeField]
    private GameObject CorridorParent;
    [SerializeField]
    private GameObject PeopleParent;


    [Header("Floor")]
    [SerializeField]
    private GameObject Floor;
    [SerializeField]
    private GameObject[] FloorGreen;
    [SerializeField]
    private GameObject[] FloorBlue;

    private GameObject selectedFoor;

    private GameObject SpawnObject;

    [SerializeField]
    private GameObject Null;
    [Space(30)]
    [Header("Editor")]
    //sets the size of the building in the editor
    [SerializeField]
    private Vector2 buildingSize;
    [Range(1, 6)]
    [SerializeField]
    private int corridorAmount;
    [SerializeField]
    private bool loopGeneration;
    [SerializeField]
    private bool randomDifficulty;
    [SerializeField]
    private int debugDifficulty;


    private GameObject[,] tiles;

    //do i even need this?
    private int count = 0;
    [Space(30)]
    [SerializeField]
    List<GameObject> nullTiles;

    [SerializeField]
    private int difficulty;


    //corridor variables
    private int startPointY;
    private int startPointX;

    [Header("Potential Doors")]
    [SerializeField]
    private List<GameObject> potentialDoors = new List<GameObject>();

    [Header("Room Variables")]
    //[SerializeField]
    //private GameObject room1;
    [SerializeField]
    private int roomNum = 1;

    private GameObject tempDoor;
    private int tempDoorX;
    private int tempDoorY;

    [Header("Enemy Variables")]
    [SerializeField]
    private GameObject basicEnemy;

    [Header("Hostage Variables")]
    [SerializeField]
    private GameObject basicHostage;

    private GameObject currentHostage;

    // Use this for initialization
    void Start()
    {
        PavementParent = GameObject.Find("Pavement");
        RoadParent = GameObject.Find("Road");

        WallsParent = GameObject.Find("Walls");
        CorridorParent = GameObject.Find("Corridor");
        PeopleParent = GameObject.Find("People");


        //just sets difficulty to easy if it can't find the gamemanager object, should only happen if i open the game straight from the test scene.
        if (GameObject.Find("GameManager") != null)
            difficulty = GameObject.Find("GameManager").GetComponent<GameSettings>().GetDifficulty();
        else
        {
            if (!randomDifficulty)
            {
                difficulty = debugDifficulty;
                if (difficulty < 0)
                    difficulty = 0;
                if (difficulty > 2)
                    difficulty = 2;
            }
            else
            {
                difficulty = Random.Range(0, 2);
            }
        }

        switch (difficulty)
        {
            case 0:
                buildingSize.x = 15;
                buildingSize.y = 10;
                break;
            case 1:
                buildingSize.x = 20;
                buildingSize.y = 15;
                break;
            case 2:
                buildingSize.x = 25;
                buildingSize.y = 20;
                break;
        }

        tiles = new GameObject[(int)buildingSize.x, (int)buildingSize.y];
        SpawnObject = Null;

        for (int i = 0; i < buildingSize.x; i++)
        {
            for (int j = 0; j < buildingSize.y; j++)
            {

                //spawns each tile
                //0.63 because sprites are 64x64
                //0.64 causes a few visual issues, so set to 0.63 to move tiles slightly closer together
                GameObject temp = Instantiate(SpawnObject, new Vector3(i * 0.63f, j * 0.63f, 0), Quaternion.identity) as GameObject;
                //set name of tile to its position
                temp.name = i.ToString() + "_" + j.ToString();
                temp.AddComponent<Node>();
                temp.GetComponent<Node>().SetPos(new Vector2(i, j));
                temp.GetComponent<Node>().setWeighting(5);
                //add tile to array 
                tiles[i, j] = temp;
                count++;

                //changing sprites
                //checks the current tile's position, changes the sprites to wall sprites if needed

                if (i == 0 || i == buildingSize.x - 1)
                {
                    ChangeSprite(temp, WallVertical);
                    temp.transform.parent = WallsParent.transform;
                    temp.GetComponent<Node>().setWeighting(999);

                }
                if (j == 0 || j == buildingSize.y - 1)
                {
                    ChangeSprite(temp, WallHorizontal);
                    temp.transform.parent = WallsParent.transform;
                    temp.GetComponent<Node>().setWeighting(999);
                }

                //corner sprites
                if (i == 0 && j == 0)
                {
                    ChangeSprite(temp, WallCornerBottomLeft);
                    temp.transform.parent = WallsParent.transform;
                    temp.GetComponent<Node>().setWeighting(999);
                }
                else if (i == 0 && j == (buildingSize.y - 1))
                {
                    ChangeSprite(temp, WallCornerTopLeft);
                    temp.transform.parent = WallsParent.transform;
                    temp.GetComponent<Node>().setWeighting(999);
                }
                else if (i == (buildingSize.x - 1) && j == 0)
                {
                    ChangeSprite(temp, WallCornerBottomRight);
                    temp.transform.parent = WallsParent.transform;
                    temp.GetComponent<Node>().setWeighting(999);
                }
                else if (i == (buildingSize.x - 1) && j == (buildingSize.y - 1))
                {
                    ChangeSprite(temp, WallCornerTopRight);
                    temp.transform.parent = WallsParent.transform;
                    temp.GetComponent<Node>().setWeighting(999);
                }

            }
        }

        HallwayGeneration();
        RoomGeneration();
        SplitLargeRooms();
        PlaceHostage();
        PlaceEnemies();

        PlaceExterior();

        if (loopGeneration)
        {
            Application.LoadLevel(3);
        }
    }

    void ChangeSprite(GameObject objectToChange, GameObject spriteToChangeTo)
    {
        if (!objectToChange.CompareTag("Door"))
        {
            objectToChange.GetComponent<SpriteRenderer>().sprite = spriteToChangeTo.GetComponent<SpriteRenderer>().sprite;
            if (!objectToChange.CompareTag("NotDoor"))
                objectToChange.tag = spriteToChangeTo.tag;
        }
    }

    void HallwayGeneration()
    {
        bool mainHallwayIsHorizontal;

        if (Random.value >= 0.5)
            mainHallwayIsHorizontal = true;
        else
            mainHallwayIsHorizontal = false;

        if (corridorAmount > 0)
        {
            for (int i = 0; i < corridorAmount; i++)
            {
                //build main corridor
                if (i == 0)
                {
                    //if the main corridor is horizontal
                    if (mainHallwayIsHorizontal)
                    {
                        //start point, random tile on Y axis
                        startPointY = Random.Range(3, (int)buildingSize.y - 3);

                        //left wall door
                        AddPotentialDoor(0, startPointY);
                        //potentialDoors.Add(GameObject.Find(0 + "_" + startPointY.ToString()));
                        //potentialDoor1 = GameObject.Find(0 + "_" + startPointY.ToString());
                        //right wall door
                        AddPotentialDoor((int)buildingSize.x - 1, startPointY);
                        //potentialDoors.Add(GameObject.Find((buildingSize.x - 1).ToString() + "_" + startPointY.ToString()));
                        // potentialDoor2 = GameObject.Find((buildingSize.x - 1).ToString() + "_" + startPointY.ToString());

                        //from the left wall, at the set Y position, move right, changing the tiles to floors
                        for (int j = 1; j < buildingSize.x - 1; j++)
                        {
                            GameObject temp = GameObject.Find(j.ToString() + "_" + startPointY.ToString());
                            ChangeSprite(temp, Floor);
                            temp.GetComponent<Node>().SetIsTaken(false);
                            temp.transform.parent = CorridorParent.transform;
                        }
                    }
                    else
                    {
                        //start point, random tile on X axis
                        startPointX = Random.Range(3, (int)buildingSize.x - 3);

                        //top wall door
                        //potentialDoor1 = GameObject.Find(startPointX.ToString() + "_" + (buildingSize.y - 1).ToString());
                        AddPotentialDoor(startPointX, (int)buildingSize.y - 1);
                        //potentialDoors.Add(GameObject.Find(startPointX.ToString() + "_" + (buildingSize.y - 1).ToString()));
                        //bottom wall door
                        //potentialDoor2 = GameObject.Find(startPointX.ToString() + "_" + 0);
                        AddPotentialDoor(startPointX, 0);
                        //potentialDoors.Add(GameObject.Find(startPointX.ToString() + "_" + 0));

                        //from the set x position & set y position, move up, changing the tiles to floors

                        for (int j = 1; j < buildingSize.y - 1; j++)
                        {
                            GameObject temp = GameObject.Find(startPointX.ToString() + "_" + j.ToString());
                            ChangeSprite(temp, Floor);
                            temp.GetComponent<Node>().SetIsTaken(false);
                            temp.transform.parent = CorridorParent.transform;
                        }
                    }
                }
                else
                {
                    if (mainHallwayIsHorizontal)
                    {
                        //main hallway is along the x axis

                        //need to update so it cant spawn next corridor either on the same point as previous one, or 2 tiles away
                        startPointX = Random.Range(3, (int)buildingSize.x - 3);

                        //randomly decide to generate corridor up or down from the main corridor
                        if (Random.value >= 0.5)
                        {
                            if (i == 1)
                            {
                                //potentialDoor3 = GameObject.Find(startPointX.ToString() + "_" + (buildingSize.y - 1).ToString());
                                AddPotentialDoor(startPointX, (int)buildingSize.y - 1);
                                //potentialDoors.Add(GameObject.Find(startPointX.ToString() + "_" + (buildingSize.y - 1).ToString()));
                            }
                            else
                            {
                                //potentialDoor4 = GameObject.Find(startPointX.ToString() + "_" + (buildingSize.y - 1).ToString());
                                AddPotentialDoor(startPointX, (int)buildingSize.y - 1);
                                //potentialDoors.Add(GameObject.Find(startPointX.ToString() + "_" + (buildingSize.y - 1).ToString()));
                            }

                            for (int j = startPointY; j < buildingSize.y - 1; j++)
                            {
                                GameObject temp = GameObject.Find(startPointX.ToString() + "_" + j.ToString());
                                ChangeSprite(temp, Floor);
                                temp.GetComponent<Node>().SetIsTaken(false);
                                temp.transform.parent = CorridorParent.transform;
                            }
                        }
                        else
                        {
                            if (i == 1)
                            {
                                //potentialDoor3 = GameObject.Find(startPointX.ToString() + "_" + 0);
                                AddPotentialDoor(startPointX, 0);
                                //potentialDoors.Add(GameObject.Find(startPointX.ToString() + "_" + 0));
                            }
                            else
                            {
                                //potentialDoor4 = GameObject.Find(startPointX.ToString() + "_" + 0);
                                AddPotentialDoor(startPointX, 0);
                                //potentialDoors.Add(GameObject.Find(startPointX.ToString() + "_" + 0));
                            }

                            for (int j = startPointY; j > 0; j--)
                            {
                                GameObject temp = GameObject.Find(startPointX.ToString() + "_" + j.ToString());
                                ChangeSprite(temp, Floor);
                                temp.GetComponent<Node>().SetIsTaken(false);
                                temp.transform.parent = CorridorParent.transform;
                            }
                        }


                    }
                    else
                    {
                        //main hallway is along the y axis

                        //need to update so it cant spawn next corridor either on the same point as previous one, or 2 tiles away
                        startPointY = Random.Range(3, (int)buildingSize.y - 3);

                        //randomly decide to generate corridor left or right from the main corridor
                        if (Random.value >= 0.5)
                        {
                            if (i == 1)
                            {
                                //potentialDoor3 = GameObject.Find((buildingSize.x - 1).ToString() + "_" + startPointY.ToString());
                                AddPotentialDoor((int)buildingSize.x - 1, startPointY);
                                //potentialDoors.Add(GameObject.Find((buildingSize.x - 1).ToString() + "_" + startPointY.ToString()));
                            }
                            else
                            {
                                //potentialDoor4 = GameObject.Find((buildingSize.x - 1).ToString() + "_" + startPointY.ToString());
                                AddPotentialDoor((int)buildingSize.x - 1, startPointY);
                                //potentialDoors.Add(GameObject.Find((buildingSize.x - 1).ToString() + "_" + startPointY.ToString()));
                            }

                            for (int j = startPointX; j < buildingSize.x - 1; j++)
                            {
                                GameObject temp = GameObject.Find(j.ToString() + "_" + startPointY.ToString());
                                ChangeSprite(temp, Floor);
                                temp.GetComponent<Node>().SetIsTaken(false);
                                temp.transform.parent = CorridorParent.transform;
                            }
                        }
                        else
                        {
                            if (i == 1)
                            {
                                //potentialDoor3 = GameObject.Find(0 + "_" + startPointY.ToString());
                                AddPotentialDoor(0, startPointY);
                                //potentialDoors.Add(GameObject.Find(0 + "_" + startPointY.ToString()));
                            }
                            else
                            {
                                //potentialDoor4 = GameObject.Find(0 + "_" + startPointY.ToString());
                                AddPotentialDoor(0, startPointY);
                                //potentialDoors.Add(GameObject.Find(0 + "_" + startPointY.ToString()));
                            }

                            for (int j = startPointX; j > 0; j--)
                            {
                                GameObject temp = GameObject.Find(j.ToString() + "_" + startPointY.ToString());
                                ChangeSprite(temp, Floor);
                                temp.GetComponent<Node>().SetIsTaken(false);
                                temp.transform.parent = CorridorParent.transform;
                            }
                        }
                    }
                }
            }
        }

        //choose where to place door and cleans up walls next to door
        //tag doors that arent used as "NotDoor" to potentially be used in the future


        int rand = Random.Range(0, potentialDoors.Count);

        for (int i = 0; i < potentialDoors.Count; i++)
        {
            if (i == rand)
            {
                ChangeSprite(potentialDoors[i], Floor);
                FrontDoorWalls(potentialDoors[i].name);
                potentialDoors[i].tag = "Door";
                potentialDoors[i].GetComponent<Node>().setWeighting(5);
            }
            else
            {
                potentialDoors[i].tag = "NotDoor";
            }
        }

        //fix tiles where doors aren't used
        NotDoorWalls();
    }
    //update to use actual location not string
    void FrontDoorWalls(string location)
    {

        for (int i = 0; i < buildingSize.x; i++)
        {
            for (int j = 0; j < buildingSize.y; j++)
            {
                if (tiles[i, j].name == location)
                {
                    if (i == 0)
                    {
                        ChangeSprite(tiles[i, (j + 1)], WallCornerBottomLeft);
                        ChangeSprite(tiles[i, (j - 1)], WallCornerTopLeft);
                    }
                    else if (i == (buildingSize.x - 1))
                    {
                        ChangeSprite(tiles[i, (j + 1)], WallCornerBottomRight);
                        ChangeSprite(tiles[i, (j - 1)], WallCornerTopRight);
                    }

                    else if (j == 0)
                    {
                        ChangeSprite(tiles[i + 1, j], WallCornerBottomLeft);
                        ChangeSprite(tiles[i - 1, j], WallCornerBottomRight);
                    }
                    else if (j == (buildingSize.y - 1))
                    {
                        ChangeSprite(tiles[i + 1, j], WallCornerTopLeft);
                        ChangeSprite(tiles[i - 1, j], WallCornerTopRight);
                    }
                }
            }
        }


    }

    void InteriorDoorWalls(int x, int y)
    {
        //get each interior door
        //get walls on either side
        //check surroundings to decide what sprites to use

        //currently checks left and right of door due to doors only being places horizontally
        //WILL NEED UPDATING

        //if tile to the right of door is a wall
        if (tiles[x + 1, y].tag == "Wall")
        {
            //if tile above wall is floor
            //currently code is being ran before rooms are filled with floor, meaning null tags much also be included
            if (tiles[x + 1, y + 1].tag == "Floor" || tiles[x + 1, y + 1].tag == "Null")
            {
                //if tiles below are also floor
                if (tiles[x + 1, y - 1].tag == "Floor" || tiles[x + 1, y - 1].tag == "Null")
                {
                    ChangeSprite(tiles[x + 1, y], WallEdgeRight);
                }
                else
                {
                    ChangeSprite(tiles[x + 1, y], WallEdgeBottom);
                }
            }
            //if tile above wall is wall
            else
            {
                //if tile below is floor
                if (tiles[x + 1, y - 1].tag == "Floor" || tiles[x + 1, y - 1].tag == "Null")
                {
                    ChangeSprite(tiles[x + 1, y], WallEdgeTop);
                }
                //if both above and below are walls
                else
                {
                    ChangeSprite(tiles[x + 1, y], WallVertical);
                }
            }
        }
        //if tile to left of wall is a wall
        if (tiles[x - 1, y].tag == "Wall")
        {
            //if tile above wall is floor
            //currently code is being ran before rooms are filled with floor, meaning null tags much also be included
            if (tiles[x - 1, y + 1].tag == "Floor" || tiles[x - 1, y + 1].tag == "Null")
            {
                //if tiles below are also floor
                if (tiles[x - 1, y - 1].tag == "Floor" || tiles[x - 1, y - 1].tag == "Null")
                {
                    ChangeSprite(tiles[x - 1, y], WallEdgeLeft);
                }
                else
                {
                    ChangeSprite(tiles[x - 1, y], WallEdgeBottom);
                }
            }
            //if tile above wall is wall
            else
            {
                //if tile below is floor
                if (tiles[x - 1, y - 1].tag == "Floor" || tiles[x - 1, y - 1].tag == "Null")
                {
                    ChangeSprite(tiles[x - 1, y], WallEdgeTop);
                }
                //if both above and below are walls
                else
                {
                    ChangeSprite(tiles[x - 1, y], WallVertical);
                }
            }
        }
    }

    void AddPotentialDoor(int x, int y)
    {
        if (potentialDoors.Count != 0)
        {
            bool temp = false;
            for (int i = 0; i < potentialDoors.Count; i++)
            {
                if (potentialDoors[i] == tiles[x,y])
                {
                    temp = true;
                }
            }

            if (!temp)
            {
                potentialDoors.Add(tiles[x, y]);
            }
        }
        else
        {
            potentialDoors.Add(tiles[x, y]);
        }
    }

    void NotDoorWalls()
    {

        //loop through tiles, find ones tagged "NotDoor", check their position, change tiles around them accordingly
        for (int i = 0; i <= buildingSize.x - 1; i++)
        {
            for (int j = 0; j <= buildingSize.y - 1; j++)
            {
                if (tiles[i, j].tag == "NotDoor")
                {

                    if (i == 0)
                    {
                        ChangeSprite(tiles[i, j + 1], WallTriLeft);
                        ChangeSprite(tiles[i, j - 1], WallTriLeft);
                        tiles[i, j + 1].transform.parent = WallsParent.transform;
                        tiles[i, j - 1].transform.parent = WallsParent.transform;
                        tiles[i, j + 1].GetComponent<Node>().setWeighting(999);
                        tiles[i, j - 1].GetComponent<Node>().setWeighting(999);
                    }
                    else if (i == buildingSize.x - 1)
                    {
                        ChangeSprite(tiles[i, j + 1], WallTriRight);
                        ChangeSprite(tiles[i, j - 1], WallTriRight);
                        tiles[i, j + 1].transform.parent = WallsParent.transform;
                        tiles[i, j - 1].transform.parent = WallsParent.transform;
                        tiles[i, j + 1].GetComponent<Node>().setWeighting(999);
                        tiles[i, j - 1].GetComponent<Node>().setWeighting(999);
                    }
                    else if (j == 0)
                    {
                        ChangeSprite(tiles[i + 1, j], WallTriBottom);
                        ChangeSprite(tiles[i - 1, j], WallTriBottom);
                        tiles[i + 1, j].transform.parent = WallsParent.transform;
                        tiles[i - 1, j].transform.parent = WallsParent.transform;
                        tiles[i + 1, j].GetComponent<Node>().setWeighting(999);
                        tiles[i - 1, j].GetComponent<Node>().setWeighting(999);
                    }
                    else if (j == buildingSize.y - 1)
                    {
                        ChangeSprite(tiles[i + 1, j], WallTriTop);
                        ChangeSprite(tiles[i - 1, j], WallTriTop);
                        tiles[i + 1, j].transform.parent = WallsParent.transform;
                        tiles[i - 1, j].transform.parent = WallsParent.transform;
                        tiles[i + 1, j].GetComponent<Node>().setWeighting(999);
                        tiles[i - 1, j].GetComponent<Node>().setWeighting(999);
                    }
                    //SurroundingTilesSpriteChange(i, j);
                }
            }
        }
    }

    void RoomGeneration()
    {

        for (int i = 0; i < buildingSize.x; i++)
        {
            for (int j = 0; j < buildingSize.y; j++)
            {
                //if tile isnt a wall or hallway
                if (tiles[i, j].tag == "Null")
                {
                    //check nearby tiles to change sprites to corners if needed
                    if (tiles[i + 1, j].tag == "Floor")
                    {
                        if (tiles[i, j + 1].tag == "Floor")
                        {
                            ChangeSprite(tiles[i, j], WallCornerTopRight);
                            tiles[i, j].transform.parent = WallsParent.transform;
                            tiles[i, j].GetComponent<Node>().setWeighting(999);
                        }
                        else if (tiles[i, j - 1].tag == "Floor")
                        {
                            ChangeSprite(tiles[i, j], WallCornerBottomRight);
                            tiles[i, j].transform.parent = WallsParent.transform;
                            tiles[i, j].GetComponent<Node>().setWeighting(999);
                        }
                    }
                    else if (tiles[i - 1, j].tag == "Floor")
                    {
                        if (tiles[i, j + 1].tag == "Floor")
                        {
                            ChangeSprite(tiles[i, j], WallCornerTopLeft);
                            tiles[i, j].transform.parent = WallsParent.transform;
                            tiles[i, j].GetComponent<Node>().setWeighting(999);
                        }
                        else if (tiles[i, j - 1].tag == "Floor")
                        {
                            ChangeSprite(tiles[i, j], WallCornerBottomLeft);
                            tiles[i, j].transform.parent = WallsParent.transform;
                            tiles[i, j].GetComponent<Node>().setWeighting(999);
                        }
                    }
                }
            }
        }


        for (int i = 0; i < buildingSize.x; i++)
        {
            for (int j = 0; j < buildingSize.y; j++)
            {
                if (tiles[i, j].tag == "Null")
                {
                    if (tiles[i + 1, j].tag == "Floor" || tiles[i - 1, j].tag == "Floor")
                    {
                        ChangeSprite(tiles[i, j], WallVertical);
                        tiles[i, j].transform.parent = WallsParent.transform;
                        tiles[i, j].GetComponent<Node>().setWeighting(999);
                    }
                    else if (tiles[i, j + 1].tag == "Floor" || tiles[i, j - 1].tag == "Floor")
                    {
                        ChangeSprite(tiles[i, j], WallHorizontal);
                        tiles[i, j].transform.parent = WallsParent.transform;
                        tiles[i, j].GetComponent<Node>().setWeighting(999);
                    }
                }
            }
        }

        //doors

        bool roomsMade = false;

        while (!roomsMade)
        {
            nullTiles = new List<GameObject>();

            //int count = 0;

            for (int j = 0; j < buildingSize.x - 1; j++)
            {
                for (int k = 0; k < buildingSize.y - 1; k++)
                {
                    if (tiles[j, k].tag == "Null")
                    {
                        //nullTiles[count] = tiles[j, k];
                        nullTiles.Add(tiles[j, k]);
                        //count++;
                    }
                }
            }

            int rand = Random.Range(0, nullTiles.Count);

            //print(rand);
            //print(nullTiles[rand]);

            //ChangeSprite(nullTiles[rand], Floor);
            if (nullTiles.Count != 0)
                FindDoorPlacement(nullTiles[rand]);
            else
                roomsMade = true;
            ColliderRemoval();
        }

    }

    void FindDoorPlacement(GameObject tile)
    {
        int tileX = -1;
        int tileY = -1;

        for (int i = 0; i < buildingSize.x - 1; i++)
        {
            for (int j = 0; j < buildingSize.y - 1; j++)
            {
                if (tiles[i, j] == tile)
                {
                    //finds tileInArray
                    tileX = i;
                    tileY = j;
                }
            }
        }

        //try finding door above tile
        bool found = false;

        for (int i = tileY + 1; i < buildingSize.y; i++)
        {
            if (tiles[tileX, i].tag == "Floor")
            {
                //print("found door placement: " + tiles[tileX, i - 1]);

                ChangeSprite(tiles[tileX, i - 1], Floor);
                tiles[tileX, i - 1].tag = "Door";
                tiles[tileX, i - 1].GetComponent<Node>().setWeighting(5);
                tempDoor = tiles[tileX, i - 1];
                tempDoorX = tileX;
                tempDoorY = i - 1;
                InteriorDoorWalls(tileX, i - 1);

                RoomFloorFill(tileX, tileY);
                found = true;
                break;
            }
        }

        //if door couldnt be found above
        //try below
        if (!found)
        {
            for (int i = tileY - 1; i > 0; i--)
            {

                if (tiles[tileX, i].tag == "Floor")
                {
                    //print("found door placement: " + tiles[tileX, i + 1]);

                    ChangeSprite(tiles[tileX, i + 1], Floor);
                    tiles[tileX, i + 1].tag = "Door";
                    tiles[tileX, i + 1].GetComponent<Node>().setWeighting(5);
                    tempDoor = tiles[tileX, i + 1];
                    tempDoorX = tileX;
                    tempDoorY = i + 1;
                    InteriorDoorWalls(tileX, i + 1);

                    RoomFloorFill(tileX, tileY);
                    found = true;
                    break;
                }
            }
        }

        //implement code to try left/right, randomly choose which one to do first
        //very rough code, will need to be cleaned up

        if (!found)
        {
            if (Random.value >= 0.5)
            {
                //check left
                for (int i = tileX - 1; i > 0; i--)
                {
                    if (tiles[i, tileY].tag == "Floor")
                    {
                        ChangeSprite(tiles[i + 1, tileY], Floor);
                        tiles[i + 1, tileY].tag = "Door";
                        tiles[i + 1, tileY].GetComponent<Node>().setWeighting(5);
                        tempDoor = tiles[i + 1, tileY];
                        tempDoorX = i + 1;
                        tempDoorY = tileY;
                        InteriorDoorWalls(i + 1, tileY);

                        RoomFloorFill(tileX, tileY);
                        found = true;
                        break;
                    }
                }
            }
            else
            {
                //check right
                for (int i = tileX + 1; i < buildingSize.x - 1; i++)
                {
                    if (tiles[i, tileY].tag == "Floor")
                    {
                        ChangeSprite(tiles[i - 1, tileY], Floor);
                        tiles[i - 1, tileY].tag = "Door";
                        tiles[i - 1, tileY].GetComponent<Node>().setWeighting(5);
                        tempDoor = tiles[i - 1, tileY];
                        tempDoorX = i - 1;
                        tempDoorY = tileY;
                        InteriorDoorWalls(i - 1, tileY);

                        RoomFloorFill(tileX, tileY);
                        found = true;
                        break;
                    }
                }
            }
        }


    }

    void RoomFloorFill(int x, int y)
    {
        if (transform.childCount == roomNum - 1)
        {
            GameObject tempRoom;
            tempRoom = new GameObject("Room" + roomNum.ToString());
            tempRoom.AddComponent<RoomInfo>();
            tempRoom.transform.parent = transform;
        }

        //find leftmost tile in room
        for (int i = x - 1; i < buildingSize.x; i--)
        {
            if (tiles[i, y].tag == "Null")
            {
                //ChangeSprite(tiles[i, y], Floor);
            }
            else
            {
                x = i + 1;
                break;
            }
        }

        //find bottommost tile in room
        for (int i = y - 1; i < buildingSize.y; i--)
        {
            if (tiles[x, i].tag == "Null")
            {
                //ChangeSprite(tiles[x, i], Floor);
            }
            else
            {
                y = i + 1;
                break;
            }
        }

        //print tile location
        //print(x + "_" + y);

        //find right limit of room
        int limit = 0;

        for (int i = x; i < buildingSize.x; i++)
        {
            if (tiles[i, y].tag != "Null")
            {
                limit = i;
                break;
            }
        }

        //find top limit of room
        int yLimit = 0;
        
        for (int i = y; i < buildingSize.y; i++)
        {
            if (tiles[x, i].tag != "Null")
            {
                yLimit = i;
                break;
            }
        }

        //selects floor to use
        int rand = Random.Range(0, 3);
        if (rand == 0)
            selectedFoor = Floor;
        else if (rand == 1)
            selectedFoor = FloorGreen[0];
        else
            selectedFoor = FloorBlue[0];

        //nested for loop to change tiles in room to floor
        for (int i = x; i < limit; i++)
        {
            for (int j = y; j < yLimit; j++)
            {
                if (tiles[i, j].tag == "Null")
                {
                    //if selected floor wasnt the normal one
                    if (rand != 0)
                    {
                        //randomly pick which floor to use (for variety)
                        int rand2 = Random.Range(0, 2);
                        if (rand == 1)
                            selectedFoor = FloorGreen[rand2];
                        else
                            selectedFoor = FloorBlue[rand2];
                    }

                    ChangeSprite(tiles[i, j], selectedFoor);
                    tiles[i, j].GetComponent<Node>().SetIsTaken(false);

                    //populate room object
                    tiles[i, j].transform.parent = GameObject.Find("Room" + roomNum.ToString()).transform;

                    //if want to hide room interior, instansiate black block here
                }
                else
                    break;
            }
        }
        GameObject.Find("Room" + roomNum.ToString()).GetComponent<RoomInfo>().SetWidth(limit - x);
        GameObject.Find("Room" + roomNum.ToString()).GetComponent<RoomInfo>().SetHeight(yLimit - y);
        GameObject.Find("Room" + roomNum.ToString()).GetComponent<RoomInfo>().GenerateArea();
        GameObject.Find("Room" + roomNum.ToString()).GetComponent<RoomInfo>().SetDoorTile(tempDoor,tempDoorX,tempDoorY);

        int middleX = (x + limit) / 2;
        int middleY = (y + yLimit) / 2;

        //print("Room num: " + roomNum);
        //print("X : " + x + " X Limit: " + limit);
        //print("Middle X? " + middleX);
        //print("Middle Y? " + middleY);
        //print("Y : " + y + " Y Limit: " + yLimit);
        //print("MIDDLE TILE? " + tiles[middleX,middleY].name);

        GameObject.Find("Room" + roomNum.ToString()).GetComponent<RoomInfo>().SetMiddleTile(tiles[middleX, middleY], middleX, middleY);

        roomNum++;
    }

    void ColliderRemoval()
    {
        //removes colliders for floor and doorways
        foreach (GameObject g in tiles)
        {
            if (g.tag == "Floor" || g.tag == "Door")
                g.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
    void ColliderRemoval(GameObject g)
    {
        g.GetComponent<BoxCollider2D>().enabled = false;
    }

    void EnableCollider(GameObject g)
    {
        g.GetComponent<BoxCollider2D>().enabled = true;
    }

    void SurroundingTilesSpriteChange(int x, int y)
    {
        //if tile is on the very left
        if (x == 0)
        {
            //if tile to the right is floor
            if (tiles[x + 1, y].CompareTag("Floor"))
            {
                //if tile above is either wall or notdoor
                if (tiles[x,y+1].CompareTag("Wall") || tiles[x, y + 1].CompareTag("NotDoor"))
                {
                    //if tile below is either wall or notdoor
                    if (tiles[x, y - 1].CompareTag("Wall") || tiles[x, y - 1].CompareTag("NotDoor"))
                    {
                        //print("making a vertical wall at: " + x.ToString() + "," + y.ToString());
                        ChangeSprite(tiles[x, y], WallVertical);
                    }
                    else
                    {
                        //print("making a corner wall at: " + x.ToString() + "," + y.ToString());
                        ChangeSprite(tiles[x, y], WallCornerBottomLeft);
                    }
                }
                else
                {
                    //if tile below is either wall or notdoor
                    if (tiles[x, y - 1].CompareTag("Wall") || tiles[x, y - 1].CompareTag("NotDoor"))
                    {
                        //print("making a corner wall at: " + x.ToString() + "," + y.ToString());
                        ChangeSprite(tiles[x, y], WallCornerTopLeft);
                    }
                    else
                    {
                        //print("making a corner wall at: " + x.ToString() + "," + y.ToString());
                        //ChangeSprite(tiles[x, y], WallCornerBottomLeft);
                    }
                }
            }
        }
        else if (x == buildingSize.x - 1)
        {
            
        }
    }

    void CheckSurroundingChange(int x, int y)
    {
        if (tiles[x, y + 1].CompareTag("Wall"))
        {

        }
    }

    void SplitLargeRooms()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<RoomInfo>().GetArea() >= 25)
            {
                RoomInfo roominfo = transform.GetChild(i).GetComponent<RoomInfo>();

                // find middle of room, split along there
                //need to consider if door is also in middle
                if (roominfo.GetWidth() > roominfo.GetHeight())
                {
                    //move up/down
                    //from middle tile till when??
                    //print(roominfo.GetHeight() / 2);
                    //print(roominfo.GetHeight());


                    //if door tile.x == middletile.x, randomly move middle tile left/right
                    if (roominfo.GetDoorX() == roominfo.GetMiddleX())
                    {
                        //print("he's in the way sir");
                        if (Random.value >= 0.5)
                        {
                            //roominfo.SetMiddleX(roominfo.GetMiddleX() - 1);
                            roominfo.SetMiddleTile(tiles[roominfo.GetMiddleX() - 1, roominfo.GetMiddleY()], roominfo.GetMiddleX() - 1, roominfo.GetMiddleY());
                        }
                        else
                        {
                            //roominfo.SetMiddleX(roominfo.GetMiddleX() + 1);
                            roominfo.SetMiddleTile(tiles[roominfo.GetMiddleX() + 1, roominfo.GetMiddleY()], roominfo.GetMiddleX() + 1, roominfo.GetMiddleY());
                        }
                    }

                    for (int j = 1; j <= roominfo.GetHeight() / 2; j++)
                    {
                        //if half of height is an odd number
                        //since the middle will be rounded up if even, we need to change 1 less tile up if that is the case.
                        //NEED TO CHECK IF HEIGHT == 2, IF SO, ONLY CHANGE LOWER TILE
                        if ((float)roominfo.GetHeight() % 2 != 0)
                        {
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j], WallVertical);
                            tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j].transform.parent = WallsParent.transform;
                            tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j].GetComponent<Node>().setWeighting(999);
                            EnableCollider(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j]);
                            //tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j].transform.parent = null;

                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j], WallVertical);
                            tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j].transform.parent = WallsParent.transform;
                            tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j].GetComponent<Node>().setWeighting(999);
                            EnableCollider(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j]);
                        }
                        else
                        {
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j - 1], WallVertical);
                            tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j - 1].transform.parent = WallsParent.transform;
                            tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j - 1].GetComponent<Node>().setWeighting(999);
                            EnableCollider(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j - 1]);
                            //tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + j - 1].transform.parent = null;

                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j], WallVertical);
                            tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j].transform.parent = WallsParent.transform;
                            tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j].GetComponent<Node>().setWeighting(999);
                            EnableCollider(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j]);
                        }

                        //ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j], WallVertical);
                        //tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j].transform.parent = WallsParent.transform;
                        //tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j].GetComponent<Node>().setWeighting(999);
                        //EnableCollider(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j]);
                        //tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - j].transform.parent = null;

                        if ((float)roominfo.GetHeight() % 2 != 0)
                        {
                            //print("ODD STUFF");
                            //print(roominfo.GetWidth());
                            //print(roominfo.GetWidth() / 2);
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + (roominfo.GetHeight() / 2) + 1], WallTriTop);
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - (roominfo.GetHeight() / 2) - 1], WallTriBottom);
                        }
                        else
                        {
                            //print("EVEN STUFF");
                            //print(roominfo.GetWidth());
                            //print(roominfo.GetWidth() / 2);
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + roominfo.GetHeight() / 2], WallTriTop);
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - (roominfo.GetHeight() / 2) - 1], WallTriBottom);
                        }

                        if (roominfo.GetHeight() >= 3)
                        {
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + 1], WallEdgeTop);
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - 1], WallEdgeBottom);
                        }
                        else
                        {
                            //EditorApplication.isPaused = true;
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + 1], WallHorizontal);
                            ChangeSprite(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - 1], WallEdgeBottom);
                        }
                    }
                }
                else
                {
                    //print(roominfo.GetWidth() / 2);
                    //print(roominfo.GetWidth());

                    //if door tile.y == middletile.y, randomly mode middle tile up/down
                    if (roominfo.GetDoorY() == roominfo.GetMiddleY())
                    {
                        if (Random.value >= 0.5)
                        {
                            //roominfo.SetMiddleY(roominfo.GetMiddleY() - 1);
                            roominfo.SetMiddleTile(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() - 1], roominfo.GetMiddleX(), roominfo.GetMiddleY() - 1);
                        }
                        else
                        {
                            //roominfo.SetMiddleY(roominfo.GetMiddleY() + 1);
                            roominfo.SetMiddleTile(tiles[roominfo.GetMiddleX(), roominfo.GetMiddleY() + 1], roominfo.GetMiddleX(), roominfo.GetMiddleY() + 1);
                        }
                    }

                    //move left/right
                    for (int j = 1; j <= roominfo.GetWidth() / 2; j++)
                    {
                        //if half of height is an odd number
                        //since the middle will be rounded up if even, we need to change 1 less tile up if that is the case.
                        //doesnt seem to matter with width, needs more testing
                        if ((float)roominfo.GetWidth() / 2 % 2 != 0)
                        {
                            print("odd");
                            print(roominfo.getDoorTile());
                            ChangeSprite(tiles[roominfo.GetMiddleX() + j , roominfo.GetMiddleY()], WallHorizontal);
                            tiles[roominfo.GetMiddleX() + j, roominfo.GetMiddleY()].transform.parent = WallsParent.transform;
                            tiles[roominfo.GetMiddleX() + j, roominfo.GetMiddleY()].GetComponent<Node>().setWeighting(999);
                            EnableCollider(tiles[roominfo.GetMiddleX() + j, roominfo.GetMiddleY()]);

                            ChangeSprite(tiles[roominfo.GetMiddleX() - j, roominfo.GetMiddleY()], WallHorizontal);
                            tiles[roominfo.GetMiddleX() - j, roominfo.GetMiddleY()].transform.parent = WallsParent.transform;
                            tiles[roominfo.GetMiddleX() - j, roominfo.GetMiddleY()].GetComponent<Node>().setWeighting(999);
                            EnableCollider(tiles[roominfo.GetMiddleX() - j, roominfo.GetMiddleY()]);
                            //tiles[roominfo.GetMiddleX() + j - 1, roominfo.GetMiddleY()].transform.parent = null;
                        }
                        else
                        {
                            print("even");
                            print(roominfo.getDoorTile());
                            ChangeSprite(tiles[roominfo.GetMiddleX() + j - 1, roominfo.GetMiddleY()], WallHorizontal);
                            tiles[roominfo.GetMiddleX() + j - 1, roominfo.GetMiddleY()].transform.parent = WallsParent.transform;
                            tiles[roominfo.GetMiddleX() + j - 1, roominfo.GetMiddleY()].GetComponent<Node>().setWeighting(999);
                            EnableCollider(tiles[roominfo.GetMiddleX() + j - 1, roominfo.GetMiddleY()]);

                            ChangeSprite(tiles[roominfo.GetMiddleX() - j, roominfo.GetMiddleY()], WallHorizontal);
                            tiles[roominfo.GetMiddleX() - j, roominfo.GetMiddleY()].transform.parent = WallsParent.transform;
                            tiles[roominfo.GetMiddleX() - j, roominfo.GetMiddleY()].GetComponent<Node>().setWeighting(999);
                            EnableCollider(tiles[roominfo.GetMiddleX() - j, roominfo.GetMiddleY()]);

                            //tiles[roominfo.GetMiddleX() + j - 1, roominfo.GetMiddleY()].transform.parent = null;
                        }
                    }


                    //print("SPLIT STUFF");
                    //print(roominfo.GetWidth());
                    //print(roominfo.GetWidth() / 2);

                    if ((float)roominfo.GetWidth() % 2 != 0)
                    {
                        //print("ODD STUFF");
                        //print(roominfo.GetWidth());
                        //print(roominfo.GetWidth() / 2);
                        ChangeSprite(tiles[roominfo.GetMiddleX() + (roominfo.GetWidth() / 2) + 1, roominfo.GetMiddleY()], WallTriRight);
                        ChangeSprite(tiles[roominfo.GetMiddleX() - (roominfo.GetWidth() / 2) - 1, roominfo.GetMiddleY()], WallTriLeft);
                    }
                    else
                    {
                        //print("EVEN STUFF");
                        //print(roominfo.GetWidth());
                        //print(roominfo.GetWidth() / 2);
                        ChangeSprite(tiles[roominfo.GetMiddleX() + roominfo.GetWidth() / 2, roominfo.GetMiddleY()], WallTriRight);
                        ChangeSprite(tiles[roominfo.GetMiddleX() - (roominfo.GetWidth() / 2) - 1, roominfo.GetMiddleY()], WallTriLeft);
                    }

                    if (roominfo.GetWidth() >= 3)
                    {
                        ChangeSprite(tiles[roominfo.GetMiddleX() + 1, roominfo.GetMiddleY()], WallEdgeRight);
                        ChangeSprite(tiles[roominfo.GetMiddleX() - 1, roominfo.GetMiddleY()], WallEdgeLeft);
                    }
                    else
                    {
                        //EditorApplication.isPaused = true;
                        ChangeSprite(tiles[roominfo.GetMiddleX() + 1, roominfo.GetMiddleY()], WallVertical);
                        ChangeSprite(tiles[roominfo.GetMiddleX() - 1, roominfo.GetMiddleY()], WallEdgeLeft);
                    }

                }

                ChangeSprite(roominfo.getMiddleTile(), Floor);
                ColliderRemoval(roominfo.getMiddleTile());
                roominfo.getMiddleTile().transform.parent = roominfo.transform;
                roominfo.getMiddleTile().GetComponent<Node>().setWeighting(5);

                //ChangeSprite(tiles[roominfo.GetMiddleX() + roominfo.GetWidth(), roominfo.GetMiddleY()], Floor);
                //ColliderRemoval(roominfo.getMiddleTile());
                //roominfo.getMiddleTile().transform.parent = roominfo.transform;
                //roominfo.getMiddleTile().GetComponent<Node>().setWeighting(5);
            }
        }
    }

    void PlaceEnemies()
    {
        //currently place 1 per room
        for (int i = 0; i < transform.childCount; i++)
        {

            for (int j = 0; j <= difficulty; j++)
            {
                int rand = Random.Range(0, transform.GetChild(i).childCount);
                GameObject temp = Instantiate(basicEnemy, transform.GetChild(i).GetChild(rand).transform.position, Quaternion.identity);
                temp.transform.parent = PeopleParent.transform;

                if (Application.loadedLevel != 0)
                {
                    temp.GetComponent<EnemyPathFinding>().SetPosition(transform.GetChild(i).GetChild(rand).GetComponent<Node>().GetPos());
                    temp.GetComponent<EnemyPathFinding>().SetGoal(transform.GetChild(i).GetComponent<RoomInfo>().getDoorTile(), buildingSize);
                    temp.GetComponent<EnemyPathFinding>().SetTileArray(tiles);
                    transform.GetChild(i).GetChild(rand).GetComponent<Node>().SetIsTaken(true);

                    if (difficulty == 1)
                    {
                        temp.GetComponent<EnemyHealth>().SetHealth(temp.GetComponent<EnemyHealth>().GetMaxHealth() * 2);
                    }
                    else if (difficulty == 2)
                    {
                        temp.GetComponent<EnemyHealth>().SetHealth(temp.GetComponent<EnemyHealth>().GetMaxHealth() * 3);
                    }

                    currentHostage.GetComponent<HostageInteraction>().AddEnemy(temp);
                }
            }
        }
    }

    void PlaceHostage()
    {
        bool hasSpawned = false;
        Vector3 spawnPoint;

        while (!hasSpawned)
        {
            int room = Random.Range(0, transform.childCount);
            int tileObject = Random.Range(0, transform.GetChild(room).childCount);
            GameObject tile = transform.GetChild(room).GetChild(tileObject).gameObject;
            spawnPoint = tile.transform.position;

            if (!tile.GetComponent<Node>().GetIsTaken())
            {
                currentHostage = Instantiate(basicHostage, spawnPoint, Quaternion.identity);
                currentHostage.transform.parent = PeopleParent.transform;
                hasSpawned = true;
            }
        }
    }

    void PlaceExterior()
    {
        //----------Pavement----------
        GameObject temp = Instantiate(PavementTopLeft, new Vector3(-1 * 0.63f, buildingSize.y * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = PavementParent.transform;
        temp = Instantiate(PavementTopRight, new Vector3(buildingSize.x * 0.63f, buildingSize.y * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = PavementParent.transform;
        temp = Instantiate(PavementBottomLeft, new Vector3(-1 * 0.63f, -1 * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = PavementParent.transform;
        temp = Instantiate(PavementBottomRight, new Vector3(buildingSize.x * 0.63f, -1 * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = PavementParent.transform;

        temp = Instantiate(PavementOuterCornerTopLeft, new Vector3(-5 * 0.63f, (buildingSize.y + 4) * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = PavementParent.transform;
        for (int i = 0; i < temp.transform.childCount; i++)
        {
            temp.transform.GetChild(i).GetComponent<BoxCollider2D>().enabled = true;
        }
        temp = Instantiate(PavementOuterCornerTopRight, new Vector3((buildingSize.x + 4) * 0.63f, (buildingSize.y + 4) * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = PavementParent.transform;
        for (int i = 0; i < temp.transform.childCount; i++)
        {
            temp.transform.GetChild(i).GetComponent<BoxCollider2D>().enabled = true;
        }
        temp = Instantiate(PavementOuterCornerBottomLeft, new Vector3(-5 * 0.63f, -5 * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = PavementParent.transform;
        for (int i = 0; i < temp.transform.childCount; i++)
        {
            temp.transform.GetChild(i).GetComponent<BoxCollider2D>().enabled = true;
        }
        temp = Instantiate(PavementOuterCornerBottomRight, new Vector3((buildingSize.x + 4) * 0.63f, -5 * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = PavementParent.transform;
        for (int i = 0; i < temp.transform.childCount; i++)
        {
            temp.transform.GetChild(i).GetComponent<BoxCollider2D>().enabled = true;
        }


        for (int i = 0; i < buildingSize.x; i++)
        {
            temp = Instantiate(PavementHorizontal, new Vector3(i * 0.63f, -1 * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = PavementParent.transform;
            temp = Instantiate(PavementHorizontal, new Vector3(i * 0.63f, buildingSize.y * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = PavementParent.transform;

            temp = Instantiate(PavementHorizontal, new Vector3(i * 0.63f, -5 * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = PavementParent.transform;
            temp.GetComponent<BoxCollider2D>().enabled = true;
            temp = Instantiate(PavementHorizontal, new Vector3(i * 0.63f, (buildingSize.y + 4) * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = PavementParent.transform;
            temp.GetComponent<BoxCollider2D>().enabled = true;
        }

        for (int i = 0; i < buildingSize.y; i++)
        {
            temp = Instantiate(PavementVertical, new Vector3(-1 * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = PavementParent.transform;
            temp = Instantiate(PavementVertical, new Vector3(buildingSize.x * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = PavementParent.transform;

            temp = Instantiate(PavementVertical, new Vector3(-5 * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = PavementParent.transform;
            temp.GetComponent<BoxCollider2D>().enabled = true;
            temp = Instantiate(PavementVertical, new Vector3((buildingSize.x + 4) * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = PavementParent.transform;
            temp.GetComponent<BoxCollider2D>().enabled = true;
        }
        //----------------------------

        //------------Road------------


        temp = Instantiate(RoadCornerBottomLeft, new Vector3(-2 * 0.63f, -1 * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = RoadParent.transform;
        temp = Instantiate(RoadCornerBottomRight, new Vector3((buildingSize.x + 1) * 0.63f, -1 * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = RoadParent.transform;
        temp = Instantiate(RoadCornerTopLeft, new Vector3(-2 * 0.63f, (buildingSize.y + 0) * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = RoadParent.transform;
        temp = Instantiate(RoadCornerTopRight, new Vector3((buildingSize.x + 1) * 0.63f, (buildingSize.y + 0) * 0.63f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = RoadParent.transform;

        for (int i = 0; i < buildingSize.x; i++)
        {
            temp = Instantiate(RoadBlank, new Vector3(i * 0.63f, -2 * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
            temp = Instantiate(RoadLineHorizontal, new Vector3(i * 0.63f, -3 * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
            temp = Instantiate(RoadBlank, new Vector3(i * 0.63f, -4 * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;

            temp = Instantiate(RoadBlank, new Vector3(i * 0.63f, (buildingSize.y + 1) * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
            temp = Instantiate(RoadLineHorizontal, new Vector3(i * 0.63f, (buildingSize.y + 2) * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
            temp = Instantiate(RoadBlank, new Vector3(i * 0.63f, (buildingSize.y + 3) * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
        }

        for (int i = 0; i < buildingSize.y; i++)
        {
            temp = Instantiate(RoadBlank, new Vector3(-2 * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
            temp = Instantiate(RoadLineVertical, new Vector3(-3 * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
            temp = Instantiate(RoadBlank, new Vector3(-4 * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;

            temp = Instantiate(RoadBlank, new Vector3((buildingSize.x + 1) * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
            temp = Instantiate(RoadLineVertical, new Vector3((buildingSize.x + 2) * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
            temp = Instantiate(RoadBlank, new Vector3((buildingSize.x + 3) * 0.63f, i * 0.63f, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = RoadParent.transform;
        }
        //----------------------------

    }

    public Vector2 GetBuildingSize()
    {
        return buildingSize;
    }

}
