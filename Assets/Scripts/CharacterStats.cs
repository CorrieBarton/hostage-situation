﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CharacterStats : MonoBehaviour {

    [SerializeField]
    private int characterNumber;

    private int health;
    [SerializeField]
    private int maxHealth;
    [SerializeField]
    private float speed;
    private int stamina;
    [SerializeField]
    private int maxStamina;
    [SerializeField]
    private int maxAmmo;
    [SerializeField]
    private int clipSize;
    [SerializeField]
    private GunTypes gun;
    [SerializeField]
    private float sprintMultiplier;
    [SerializeField]
    private float reloadTimer;

    private CharacterSelect selectedChar;

	// Use this for initialization
	void Start () {

        try
        {
            selectedChar = GameObject.Find("CharacterSelect").GetComponent<CharacterSelect>();

            maxHealth = selectedChar.GetMaxHealth(characterNumber - 1);
            maxStamina = selectedChar.GetMaxStamina(characterNumber - 1);
            maxAmmo = selectedChar.GetMaxAmmo(characterNumber - 1);
            clipSize = selectedChar.GetClipSize(characterNumber - 1);
            speed = selectedChar.GetSpeed(characterNumber - 1);
            gun = selectedChar.GetGun(characterNumber - 1);
            sprintMultiplier = selectedChar.GetSprintMultiplier(characterNumber - 1);
            reloadTimer = selectedChar.GetReloadTimer(characterNumber - 1);
        }
        catch (Exception e)
        {
            print("caught " + e);

            maxHealth = 100;
            maxStamina = 100;
            speed = 1.5f;
            gun = GunTypes.Pistol;
            maxAmmo = 50;
            clipSize = 10;
            sprintMultiplier = 1.5f;
            reloadTimer = 1.5f;
        }

        health = maxHealth;
        stamina = maxStamina;

        GetComponent<Weapon>().enabled = true;

        //print("max health: " + gameObject.name + " " + maxHealth);
        GetComponent<Health>().SetHealth(maxHealth);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public int GetMaxAmmo()
    {
        return maxAmmo;
    }

    public float GetSpeed()
    {
        return speed;
    }

    public int GetMaxStamina()
    {
        return maxStamina;
    }

    public float GetSprintMultiplier()
    {
        return sprintMultiplier;
    }

    public int GetClipSize()
    {
        return clipSize;
    }

    public GunTypes GetGun()
    {
        return gun;
    }
    public float GetReloadTimer()
    {
        return reloadTimer;
    }
}
