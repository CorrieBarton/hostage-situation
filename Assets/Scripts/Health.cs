﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {

    [SerializeField]
    private int health;
    [SerializeField]
    private int maxHealth;

    [SerializeField]
    private Text healthText;
    [SerializeField]
    private Slider healthBar;

    [SerializeField]
    private CharacterStats stats;

    [SerializeField]
    private bool debugTakeDamage;
    [SerializeField]
    private bool debugResetHealth;

    private CameraFollow camFollow;

    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Start () {

        camFollow = Camera.main.GetComponent<CameraFollow>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        //health = stats.GetMaxHealth();
        //healthBar.maxValue = health;

        //UpdateHealthDisplay();
    }

    public void SetHealth(int value)
    {
        //print("set " + gameObject.name + (": ") + value);

        maxHealth = value;
        health = maxHealth;
        //default at 1, gets set to 0 for player1 here :S
        healthBar.maxValue = maxHealth;
        UpdateHealthDisplay();

        UpdateHealthDisplay();
    }
	
	// Update is called once per frame
	void Update () {

        //Used for testing, remove when enemies are implemented
        if (debugTakeDamage)
        {
            TakeDamage(10);
            debugTakeDamage = false;
        }

        if (debugResetHealth)
        {
            health = maxHealth;
            healthBar.maxValue = maxHealth;
            UpdateHealthDisplay();
            debugResetHealth = false;
        }

        if (healthBar.maxValue != maxHealth)
        {
            healthBar.maxValue = maxHealth;
            UpdateHealthDisplay();
        }
		
	}

    public void TakeDamage(int damage)
    {
        health -= damage;
        StartCoroutine(SpriteChange());
        if (health <= 0)
        {
            //player is dead
            health = 0;
            gameObject.SetActive(false);
            camFollow.RemovePlayer(gameObject);
        }
        UpdateHealthDisplay();
    }

    public void RecoverHealth(int amount)
    {
        health += amount;
        if (health > maxHealth)
            health = maxHealth;
        UpdateHealthDisplay();
    }

    void UpdateHealthDisplay()
    {
        if (health > 0)
            healthText.text = health.ToString();
        else
        {
            healthText.text = "KIA";
            //print("dead : " + gameObject.name);
        }
        healthBar.value = health;
    }

    IEnumerator SpriteChange()
    {
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.2f);
        spriteRenderer.color = Color.white;
    }
}
