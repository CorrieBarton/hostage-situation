﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomInfo : MonoBehaviour {


    [SerializeField]
    private int width;
    [SerializeField]
    private int height;
    [SerializeField]
    private int area;

    [SerializeField]
    private GameObject middleTile;

    private int middleX;
    private int middleY;

    [SerializeField]
    private GameObject doorTile;
    [SerializeField]
    private int doorX;
    [SerializeField]
    private int doorY;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetWidth(int value)
    {
        width = value;
    }

    public void SetHeight(int value)
    {
        height = value;
    }

    public void GenerateArea()
    {
        area = width * height;
    }

    public void SetMiddleTile(GameObject tile, int x, int y)
    {
        middleTile = tile;
        middleX = x;
        middleY = y;
    }
    public void SetMiddleX(int x)
    {
        middleX = x;
    }

    public void SetMiddleY(int y)
    {
        middleY = y;
    }

    public void SetDoorTile(GameObject tile, int x, int y)
    {
        doorTile = tile;
        doorX = x;
        doorY = y;
    }

    public int GetArea()
    {
        return area;
    }

    public GameObject getMiddleTile()
    {
        return middleTile;
    }

    public int GetWidth()
    {
        return width;
    }
    public int GetHeight()
    {
        return height;
    }

    public int GetMiddleX()
    {
        return middleX;
    }

    public int GetMiddleY()
    {
        return middleY;
    }

    public GameObject getDoorTile()
    {
        return doorTile;
    }

    public int GetDoorX()
    {
        return doorX;
    }

    public int GetDoorY()
    {
        return doorY;
    }

}
