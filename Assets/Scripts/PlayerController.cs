﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private int playerNum;

    private CharacterStats characterStats;

    private Vector2 movement;
    [SerializeField]
    private float speed;
    [SerializeField]
    private int stamina;
    [SerializeField]
    private int maxStamina;
    [SerializeField]
    private float sprintMultiplier;
    [SerializeField]
    private bool isSprinting = false;

    private Rigidbody2D rigid;

    [SerializeField]
    private Slider staminaBar;

    [SerializeField]
    private Weapon weapon;

    [SerializeField]
    private Vector2 tempMyTilePosition;
    [SerializeField]
    private Vector2 tempMyTilePositionNotRounded;

    [SerializeField]
    private bool hasHostage = false;

    private float gunTimer;
    [SerializeField]
    private float gunTimerMin;
    [SerializeField]
    private float gunTimerMax;
    [SerializeField]
    private float gunTimerCount;

    private bool tempTriggerPress = false;


    [Header("Building Values")]
    [SerializeField]
    private GenerateBuilding building;
    [SerializeField]
    private Vector2 buildingWdithHeight;

    // Use this for initialization
    void Start () {
        rigid = GetComponent<Rigidbody2D>();


        characterStats = GetComponent<CharacterStats>();
        speed = characterStats.GetSpeed();
        maxStamina = characterStats.GetMaxStamina();
        stamina = maxStamina;
        sprintMultiplier = characterStats.GetSprintMultiplier();

        staminaBar.maxValue = stamina;
        staminaBar.value = stamina;

        weapon = GetComponent<Weapon>();

        buildingWdithHeight = building.GetBuildingSize();

        //print("WEAPONS " + weapon.GetGun());

        if (weapon.GetGun() == GunTypes.MiniGun)
        {
            gunTimerMin = 0.1f;
            gunTimerMax = 0.25f;
        }
        else if (weapon.GetGun() == GunTypes.SMG)
        {
            gunTimerMin = 0.05f;
            gunTimerMax = 0.05f;
        }

        gunTimer = gunTimerMax;


    }

    void Awake()
    {
        hasHostage = false;
    }

    void Update()
    {
        //temp values
        tempMyTilePosition.x = (float)System.Math.Round(transform.position.x / 0.63f);
        tempMyTilePosition.y = (float)System.Math.Round(transform.position.y / 0.63f);

        tempMyTilePositionNotRounded.x = transform.position.x / 0.63f;
        tempMyTilePositionNotRounded.y = transform.position.y / 0.63f;

        if (hasHostage)
        {
            if (tempMyTilePosition.x < 0 || tempMyTilePosition.x > buildingWdithHeight.x || tempMyTilePosition.y < 0 || tempMyTilePosition.y > buildingWdithHeight.y)
            {
                print("you win?");
                if (GameObject.Find("GameManager") != null)
                    GameObject.Find("GameManager").GetComponent<LoadScene>().LoadNewSceneFade(1);
            }
        }


        //-------------------------------Sprinting System-----------------------------------
        if (Input.GetAxis("Sprint" + playerNum.ToString()) > 0)
        {
            isSprinting = true;
            if (stamina > 0)
            {
                stamina--;
            }
            
        }
        else
        {
            isSprinting = false;
            if (stamina < maxStamina)
            {
                //update to increase slower
                stamina++;
            }
        }

        if (stamina <= 0)
        {
            isSprinting = false;
        }

        staminaBar.value = stamina;


        //-------------------------------Weapon System---------------------------------------

        //currently only works with guns that aren't fully automatic 

        if (!weapon.GetReloading())
        {
            if (Input.GetAxis("Shoot" + playerNum.ToString()) != 0)
            {
                if (!tempTriggerPress)
                {
                    tempTriggerPress = true;

                    if (weapon.GetGun() == GunTypes.Shotgun)
                    {
                        weapon.ShootAmmo(1);
                    }
                    else
                    {
                        weapon.ShootAmmo(1);
                    }
                }
            }
            else
            {
                tempTriggerPress = false;
            }

            //if else reused code, clean up needed
            if (Input.GetAxis("Shoot" + playerNum.ToString()) != 0 && weapon.GetGun() == GunTypes.MiniGun)
            {
                //print("minigun shoot");
                if (gunTimer <= 0)
                {
                    weapon.ShootAmmo(1);
                    gunTimer = gunTimerMax - gunTimerCount;

                    gunTimerCount += 0.025f;
                    if (gunTimer < gunTimerMin)
                        gunTimer = gunTimerMin;
                }
                else
                {
                    gunTimer -= Time.deltaTime;
                }
            }
            else if (Input.GetAxis("Shoot" + playerNum.ToString()) != 0 && weapon.GetGun() == GunTypes.SMG)
            {
                print("smg shoot");
                if (gunTimer <= 0)
                {
                    weapon.ShootAmmo(1);
                    gunTimer = gunTimerMax - gunTimerCount;

                    gunTimerCount += 0.025f;
                    if (gunTimer < gunTimerMin)
                        gunTimer = gunTimerMin;
                }
                else
                {
                    gunTimer -= Time.deltaTime;
                }
            }

            if (Input.GetAxis("Shoot" + playerNum.ToString()) == 0 && weapon.GetGun() == GunTypes.MiniGun)
            {
                gunTimerCount = 0;
                gunTimer = gunTimerMax;
            }
        }
        

        if (Input.GetAxis("Reload" + playerNum.ToString()) > 0)
        {
            weapon.StartReloadAmmo();
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        //player movement
        movement.x = Input.GetAxis("Horizontal" + playerNum.ToString());
        movement.y = Input.GetAxis("Vertical" + playerNum.ToString());

        if (isSprinting)
        {
            rigid.velocity = movement * speed * sprintMultiplier;
        }
        else
        {
            rigid.velocity = movement * speed;
        }

        rigid.angularVelocity = 0.0f;

        float rx = Input.GetAxis("RightHorizontal" + playerNum.ToString());
        float ry = Input.GetAxis("RightVertical" + playerNum.ToString());

        if (rx != 0 || ry != 0)
        {
            float heading = Mathf.Atan2(rx, ry);

            transform.rotation = Quaternion.Euler(0, 0, heading * Mathf.Rad2Deg);
        }

    }

    public void SetHasHostage(bool flag)
    {
        hasHostage = flag;
    }

    public int GetPlayerNum()
    {
        return playerNum;
    } 
}
