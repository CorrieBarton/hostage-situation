﻿using UnityEngine;
using System.Collections;

public class WeaponStats : MonoBehaviour {

    //script for each weapon
    //how many bullets per mag
    [SerializeField]
    private int magSize;
    //how many mags can be held
    [SerializeField]
    private int maxMags;
    //how much damage the gun does
    [SerializeField]
    private float damage;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
